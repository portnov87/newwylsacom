/**
 * @package     EmbedPress
 * @author      PressShack <help@pressshack.com>
 * @copyright   Copyright (C) 2017 PressShack. All rights reserved.
 * @license     GPLv2 or later
 * @since       1.7.0
 */
(function($) {
    "use strict";

    $(function() {
        $('.color-field').wpColorPicker();
    });
})(jQuery);
