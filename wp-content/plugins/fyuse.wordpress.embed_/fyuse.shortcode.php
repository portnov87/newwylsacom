<?php
/*
Plugin Name: FYUSE EMBED for WP
Plugin URI: https://www.fyu.se/plugins
Description: Allows easy embeding of fyuses in wordpress. Eg, [fyuse id='tce5ex3sny' width='320' height='568']
Version: 0.2
Author: Fyusion Inc.
Author URI: https://fyusion.com
*/

add_shortcode ('fyuse', 'fyuse_handler');

function fyuse_handler ($arr)
{
  $arr = shortcode_atts (array (
    'id'     => 'wfxvg8cnpe',
    'height' => 560,
    'width'  => 320,
    'bg'     => '#333',
    'style'  => 0,
    'autoplay' => 0     
  ), $arr);

  $fyuse_output = fyuse_function ($arr);
  return ($fyuse_output);
}

function fyuse_function ($arr)
{
  $uid = $arr['id'];
  $extra = 'style="background:' . $arr['bg'] . ';"';
  $args = $arr['autoplay'] === 0 ? '' : '?autoplay=1';
  $height = $arr['height'];
  $width = $arr['width'];

  if (!$uid) return ('');

  $template = "<iframe src='https://fyu.se/v/embed/$uid$args' width='$width' height='$height' $extra frameborder='0' scrolling='no' allowfullscreen></iframe>";

  return (wp_specialchars_decode ($template));
}
?>