jQuery(document).ready(function($) {
    $('.tcode').textillate({
        loop: true,
        minDisplayTime: 5000,
        initialDelay: 800,
        autoStart: true,
        inEffects: [],
        outEffects: [],
        in: {
            effect: 'rollIn',
            delayScale: 1.5,
            delay: 50,
            sync: false,
            shuffle: true,
            reverse: false,
            callback: function() {}
        },
        out: {
            effect: 'fadeOut',
            delayScale: 1.5,
            delay: 50,
            sync: false,
            shuffle: true,
            reverse: false,
            callback: function() {}
        },
        callback: function() {}
    });
})

jQuery(document).ready(function($) {
    var thumb = jQuery('#yzthumbnail');
    var select = this.value;

    if (jQuery('#yzthumbnail').is(":checked")) {
        jQuery('.yzselectthumbtr').show();
    } else {
        jQuery('.yzselectthumbtr').hide();
    }

    thumb.change(function() {
        if (jQuery('#yzthumbnail').is(":checked")) {
            jQuery('.yzselectthumbtr').fadeIn();
        } else {
            jQuery('.yzselectthumbtr').hide();
        }
    });

    var thumb2 = jQuery('#yzthumbnailturbo');
    if (jQuery('#yzturbo').is(":checked")) {
        if (jQuery('#yzthumbnailturbo').is(":checked")) {
            jQuery('.yzselectthumbturbotr').show();
        } else {
            jQuery('.yzselectthumbturbotr').hide();
        }
    }

    thumb2.change(function() {
        if (jQuery('#yzthumbnailturbo').is(":checked")) {
            jQuery('.yzselectthumbturbotr').fadeIn();
        } else {
            jQuery('.yzselectthumbturbotr').hide();
        }
    });


    var seo = jQuery('#yzseodesc');

    if (jQuery('#yzseodesc').is(":checked")) {
        jQuery('.yzseoplugintr').show();
    } else {
        jQuery('.yzseoplugintr').hide();
    }

    seo.change(function() {
        if (jQuery('#yzseodesc').is(":checked")) {
            jQuery('.yzseoplugintr').fadeIn();
        } else {
            jQuery('.yzseoplugintr').hide();
        }
    });

    var tags = jQuery('#yzexcludetags');

    if (jQuery('#yzexcludetags').is(":checked")) {
        jQuery('.yzexcludetagslisttr').show();
    } else {
        jQuery('.yzexcludetagslisttr').hide();
    }

    tags.change(function() {
        if (jQuery('#yzexcludetags').is(":checked")) {
            jQuery('.yzexcludetagslisttr').fadeIn();
        } else {
            jQuery('.yzexcludetagslisttr').hide();
        }
    });

    var tags2 = jQuery('#yzexcludetags2');

    if (jQuery('#yzexcludetags2').is(":checked")) {
        jQuery('.yzexcludetagslist2tr').show();
    } else {
        jQuery('.yzexcludetagslist2tr').hide();
    }

    tags2.change(function() {
        if (jQuery('#yzexcludetags2').is(":checked")) {
            jQuery('.yzexcludetagslist2tr').fadeIn();
        } else {
            jQuery('.yzexcludetagslist2tr').hide();
        }
    });

    var rcont = jQuery('#yzexcludecontent');

    if (jQuery('#yzexcludecontent').is(":checked")) {
        jQuery('.yzexcludecontentlisttr').show();
    } else {
        jQuery('.yzexcludecontentlisttr').hide();
    }

    rcont.change(function() {
        if (jQuery('#yzexcludecontent').is(":checked")) {
            jQuery('.yzexcludecontentlisttr').fadeIn();
        } else {
            jQuery('.yzexcludecontentlisttr').hide();
        }
    });

    var mturbo = jQuery('#yzturbo');

    if (jQuery('#yzturbo').is(":checked")) {
        jQuery('.myturbo').show();
        if (jQuery('#yzthumbnailturbo').is(":checked")) {
            jQuery('.yzselectthumbturbotr').show();
        } else {
            jQuery('.yzselectthumbturbotr').hide();
        }

        if (jQuery('#yzrelated').is(":checked")) {
            jQuery('.yzrelatedchildtr').fadeIn();
        } else {
            jQuery('.yzrelatedchildtr').hide();
        }

    } else {
        jQuery('.myturbo').hide();
    }



    mturbo.change(function() {

        if (jQuery('#yzturbo').is(":checked")) {
            jQuery('.myturbo').fadeIn();

            if (jQuery('#yzcounterselect option:selected').val() == "Яндекс.Метрика") {
                jQuery('.metrika').fadeIn();
            } else {
                jQuery('.metrika').hide();
            }
            if (jQuery('#yzcounterselect option:selected').val() == "LiveInternet") {
                jQuery('.liveinternet').fadeIn();
            } else {
                jQuery('.liveinternet').hide();
            }
            if (jQuery('#yzcounterselect option:selected').val() == "Google Analytics") {
                jQuery('.google').fadeIn();
            } else {
                jQuery('.google').hide();
            }
            if (jQuery('#yzcounterselect option:selected').val() == "Рейтинг Mail.RU") {
                jQuery('.mailru').fadeIn();
            } else {
                jQuery('.mailru').hide();
            }
            if (jQuery('#yzcounterselect option:selected').val() == "Rambler Топ-100") {
                jQuery('.rambler').fadeIn();
            } else {
                jQuery('.rambler').hide();
            }
            if (jQuery('#yzcounterselect option:selected').val() == "Mediascope (TNS)") {
                jQuery('.mediascope').fadeIn();
            } else {
                jQuery('.mediascope').hide();
            }

            if (jQuery('#yzthumbnailturbo').is(":checked")) {
                jQuery('.yzselectthumbturbotr').fadeIn();
            } else {
                jQuery('.yzselectthumbturbotr').hide();
            }

            if (jQuery('#yzrelated').is(":checked")) {
                jQuery('.yzrelatedchildtr').fadeIn();
            } else {
                jQuery('.yzrelatedchildtr').hide();
            }

            if (jQuery('#yzad1').is(":checked")) {
                jQuery('.block1').show();
                if (jQuery('#yzad1set option:selected').val() == "РСЯ") {
                    jQuery('.trrsa').fadeIn();
                    jQuery('.trfox1').hide();
                    jQuery('.trfox2').hide();
                }
                if (jQuery('#yzad1set option:selected').val() == "ADFOX") {
                    jQuery('.trrsa').hide();
                    jQuery('.trfox1').fadeIn();
                    jQuery('.trfox2').fadeIn();
                }
            } else {
                jQuery('.block1').hide();
            }

            if (jQuery('#yzad2').is(":checked")) {
                jQuery('.block2').show();
                if (jQuery('#yzad2set option:selected').val() == "РСЯ") {
                    jQuery('.trrsa2').fadeIn();
                    jQuery('.trfox12').hide();
                    jQuery('.trfox22').hide();
                }
                if (jQuery('#yzad2set option:selected').val() == "ADFOX") {
                    jQuery('.trrsa2').hide();
                    jQuery('.trfox12').fadeIn();
                    jQuery('.trfox22').fadeIn();
                }
            } else {
                jQuery('.block2').hide();
            }

            if (jQuery('#yzad3').is(":checked")) {
                jQuery('.block3').show();
                if (jQuery('#yzad3set option:selected').val() == "РСЯ") {
                    jQuery('.trrsa3').fadeIn();
                    jQuery('.trfox13').hide();
                    jQuery('.trfox23').hide();
                }
                if (jQuery('#yzad3set option:selected').val() == "ADFOX") {
                    jQuery('.trrsa3').hide();
                    jQuery('.trfox13').fadeIn();
                    jQuery('.trfox23').fadeIn();
                }
            } else {
                jQuery('.block3').hide();
            }

        } else {
            jQuery('.myturbo').hide();
            jQuery('.yzselectthumbturbotr').hide();
        }
    });

    var imgselect = jQuery('#imgselect');
    if (jQuery('#imgselect option:selected').val() == "Указать автора") {
        jQuery('#ownname').fadeIn();
    } else {
        jQuery('#ownname').hide();
    }
    imgselect.change(function() {
        if (jQuery('#imgselect option:selected').val() == "Указать автора") {
            jQuery('#ownname').fadeIn();
        } else {
            jQuery('#ownname').hide();
        }
    });
    var capalt = jQuery('#capalt');
    if (jQuery('#capalt option:selected').val() == "Использовать alt по возможности") {
        jQuery('#altimg').fadeIn();
    } else {
        jQuery('#altimg').hide();
    }
    capalt.change(function() {
        if (jQuery('#capalt option:selected').val() == "Использовать alt по возможности") {
            jQuery('#altimg').fadeIn();
        } else {
            jQuery('#altimg').hide();
        }
    });


    var block1 = jQuery('#yzad1');
    if (jQuery('#yzturbo').is(":checked")) {
        if (jQuery('#yzad1').is(":checked")) {
            jQuery('.block1').show();
            if (jQuery('#yzad1set option:selected').val() == "РСЯ") {
                jQuery('.trrsa').fadeIn();
                jQuery('.trfox1').hide();
                jQuery('.trfox2').hide();
            }
            if (jQuery('#yzad1set option:selected').val() == "ADFOX") {
                jQuery('.trrsa').hide();
                jQuery('.trfox1').fadeIn();
                jQuery('.trfox2').fadeIn();
            }
        } else {
            jQuery('.block1').hide();
        }
    }

    block1.change(function() {
        if (jQuery('#yzad1').is(":checked")) {
            jQuery('.block1').fadeIn();
            if (jQuery('#yzad1set option:selected').val() == "РСЯ") {
                jQuery('.trrsa').fadeIn();
                jQuery('.trfox1').hide();
                jQuery('.trfox2').hide();
            }
            if (jQuery('#yzad1set option:selected').val() == "ADFOX") {
                jQuery('.trrsa').hide();
                jQuery('.trfox1').fadeIn();
                jQuery('.trfox2').fadeIn();
            }
        } else {
            jQuery('.block1').hide();
        }
    });

    jQuery(document).on('change', '#yzad1set', function() {
        if (jQuery('#yzad1set option:selected').val() == "РСЯ") {
            jQuery('.trrsa').show();
            jQuery('.trfox1').hide();
            jQuery('.trfox2').hide();
        }
        if (jQuery('#yzad1set option:selected').val() == "ADFOX") {
            jQuery('.trrsa').hide();
            jQuery('.trfox1').show();
            jQuery('.trfox2').show();
        }
    });


    var block2 = jQuery('#yzad2');

    if (jQuery('#yzturbo').is(":checked")) {
        if (jQuery('#yzad2').is(":checked")) {
            jQuery('.block2').show();
            if (jQuery('#yzad2set option:selected').val() == "РСЯ") {
                jQuery('.trrsa2').fadeIn();
                jQuery('.trfox12').hide();
                jQuery('.trfox22').hide();
            }
            if (jQuery('#yzad2set option:selected').val() == "ADFOX") {
                jQuery('.trrsa2').hide();
                jQuery('.trfox12').fadeIn();
                jQuery('.trfox22').fadeIn();
            }
        } else {
            jQuery('.block2').hide();
        }
    }

    block2.change(function() {
        if (jQuery('#yzad2').is(":checked")) {
            jQuery('.block2').fadeIn();
            if (jQuery('#yzad2set option:selected').val() == "РСЯ") {
                jQuery('.trrsa2').fadeIn();
                jQuery('.trfox12').hide();
                jQuery('.trfox22').hide();
            }
            if (jQuery('#yzad2set option:selected').val() == "ADFOX") {
                jQuery('.trrsa2').hide();
                jQuery('.trfox12').fadeIn();
                jQuery('.trfox22').fadeIn();
            }
        } else {
            jQuery('.block2').hide();
        }
    });

    jQuery(document).on('change', '#yzad2set', function() {
        if (jQuery('#yzad2set option:selected').val() == "РСЯ") {
            jQuery('.trrsa2').show();
            jQuery('.trfox12').hide();
            jQuery('.trfox22').hide();
        }
        if (jQuery('#yzad2set option:selected').val() == "ADFOX") {
            jQuery('.trrsa2').hide();
            jQuery('.trfox12').show();
            jQuery('.trfox22').show();
        }
    });

    var block3 = jQuery('#yzad3');

    if (jQuery('#yzturbo').is(":checked")) {
        if (jQuery('#yzad3').is(":checked")) {
            jQuery('.block3').show();
            if (jQuery('#yzad3set option:selected').val() == "РСЯ") {
                jQuery('.trrsa3').fadeIn();
                jQuery('.trfox13').hide();
                jQuery('.trfox23').hide();
            }
            if (jQuery('#yzad3set option:selected').val() == "ADFOX") {
                jQuery('.trrsa3').hide();
                jQuery('.trfox13').fadeIn();
                jQuery('.trfox23').fadeIn();
            }
        } else {
            jQuery('.block3').hide();
        }
    }

    block3.change(function() {
        if (jQuery('#yzad3').is(":checked")) {
            jQuery('.block3').fadeIn();
            if (jQuery('#yzad3set option:selected').val() == "РСЯ") {
                jQuery('.trrsa3').fadeIn();
                jQuery('.trfox13').hide();
                jQuery('.trfox23').hide();
            }
            if (jQuery('#yzad3set option:selected').val() == "ADFOX") {
                jQuery('.trrsa3').hide();
                jQuery('.trfox13').fadeIn();
                jQuery('.trfox23').fadeIn();
            }
        } else {
            jQuery('.block3').hide();
        }
    });

    jQuery(document).on('change', '#yzad3set', function() {
        if (jQuery('#yzad3set option:selected').val() == "РСЯ") {
            jQuery('.trrsa3').show();
            jQuery('.trfox13').hide();
            jQuery('.trfox23').hide();
        }
        if (jQuery('#yzad3set option:selected').val() == "ADFOX") {
            jQuery('.trrsa3').hide();
            jQuery('.trfox13').show();
            jQuery('.trfox23').show();
        }
    });

    if (jQuery('#yzturbo').is(":checked")) {
        if (jQuery('#yzrelated').is(":checked")) {
            jQuery('.yzrelatedchildtr').show();
        } else {
            jQuery('.yzrelatedchildtr').hide();
        }
    }
    var related = jQuery('#yzrelated');
    related.change(function() {
        if (jQuery('#yzrelated').is(":checked")) {
            jQuery('.yzrelatedchildtr').fadeIn();
        } else {
            jQuery('.yzrelatedchildtr').hide();
        }
    });

    if (jQuery('#yzturbo').is(":checked")) {
        if (jQuery('#yzcounterselect option:selected').val() == "Яндекс.Метрика") {
            jQuery('.metrika').fadeIn();
        } else {
            jQuery('.metrika').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "LiveInternet") {
            jQuery('.liveinternet').fadeIn();
        } else {
            jQuery('.liveinternet').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Google Analytics") {
            jQuery('.google').fadeIn();
        } else {
            jQuery('.google').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Рейтинг Mail.RU") {
            jQuery('.mailru').fadeIn();
        } else {
            jQuery('.mailru').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Rambler Топ-100") {
            jQuery('.rambler').fadeIn();
        } else {
            jQuery('.rambler').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Mediascope (TNS)") {
            jQuery('.mediascope').fadeIn();
        } else {
            jQuery('.mediascope').hide();
        }
    }

    var yzcounterselect = jQuery('#yzcounterselect');
    yzcounterselect.change(function() {
        if (jQuery('#yzcounterselect option:selected').val() == "Яндекс.Метрика") {
            jQuery('.metrika').fadeIn();
        } else {
            jQuery('.metrika').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "LiveInternet") {
            jQuery('.liveinternet').fadeIn();
        } else {
            jQuery('.liveinternet').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Google Analytics") {
            jQuery('.google').fadeIn();
        } else {
            jQuery('.google').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Рейтинг Mail.RU") {
            jQuery('.mailru').fadeIn();
        } else {
            jQuery('.mailru').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Rambler Топ-100") {
            jQuery('.rambler').fadeIn();
        } else {
            jQuery('.rambler').hide();
        }
        if (jQuery('#yzcounterselect option:selected').val() == "Mediascope (TNS)") {
            jQuery('.mediascope').fadeIn();
        } else {
            jQuery('.mediascope').hide();
        }
    });
    
        if (jQuery('#yzqueryselect option:selected').val() == "Все таксономии, кроме исключенных") {
            jQuery('.yztaxlisttr').fadeIn();
            jQuery('#excludespan').fadeIn();
        } else {
            jQuery('.yztaxlisttr').hide();
            jQuery('#excludespan').hide();
        }
        if (jQuery('#yzqueryselect option:selected').val() == "Только указанные таксономии") {
            jQuery('.yzaddtaxlisttr').fadeIn();
            jQuery('#includespan').fadeIn();
        } else {
            jQuery('.yzaddtaxlisttr').hide();
            jQuery('#includespan').hide();
        }
    
    var yzqueryselect = jQuery('#yzqueryselect');
    yzqueryselect.change(function() {
        if (jQuery('#yzqueryselect option:selected').val() == "Все таксономии, кроме исключенных") {
            jQuery('.yztaxlisttr').fadeIn();
            jQuery('#excludespan').fadeIn();
        } else {
            jQuery('.yztaxlisttr').hide();
            jQuery('#excludespan').hide();
        }
        if (jQuery('#yzqueryselect option:selected').val() == "Только указанные таксономии") {
            jQuery('.yzaddtaxlisttr').fadeIn();
            jQuery('#includespan').fadeIn();
        } else {
            jQuery('.yzaddtaxlisttr').hide();
            jQuery('#includespan').hide();
        }
    });


})