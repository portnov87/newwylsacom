<?php
/*
Plugin Name: RSS for Yandex Zen
Plugin URI: https://wordpress.org/plugins/rss-for-yandex-zen/
Description: Создание RSS-ленты для сервиса Яндекс.Дзен.
Version: 1.23
Author: Flector
Author URI: https://profiles.wordpress.org/flector#content-plugins
Text Domain: rss-for-yandex-zen
*/ 

//функция установки значений по умолчанию при активации плагина begin
function yzen_init() {
    $yzen_options = array();  
    $yzen_options['yzrssname'] = 'zen';
    $yzen_options['yzcategory'] = "Общество";
    $yzen_options['yzrating'] = "Нет (не для взрослых)";
    $yzen_options['yztitle'] = get_bloginfo_rss('title');
    $yzen_options['yzlink'] = get_bloginfo_rss('url');
    $yzen_options['yzdescription'] = get_bloginfo_rss('description');
    $yzen_options['yzlanguage'] = "ru";
    $yzen_options['yznumber'] = "50";
    $yzen_options['yztype'] = "post";
    $yzen_options['yzfigcaption'] = "Использовать alt по возможности";
    $yzen_options['yzimgauthorselect'] = "Автор записи";
    $yzen_options['yzimgauthor'] = "";
    $yzen_options['yzauthor'] = "";
    $yzen_options['yzthumbnail'] = "disabled";
    $yzen_options['yzselectthumb'] = "";
    $yzen_options['yzseodesc'] = "disabled";
    $yzen_options['yzseoplugin'] = "Yoast SEO";
    $yzen_options['yzexcludetags'] = "disabled";
    $yzen_options['yzexcludetagslist'] = "<div>";
    $yzen_options['yzexcludetags2'] = "enabled";
    $yzen_options['yzexcludetagslist2'] = "<iframe>,<script>,<ins>,<style>,<object>";
    $yzen_options['yzexcludecontent'] = "disabled";
    $yzen_options['yzexcludecontentlist'] = esc_textarea("<!--more-->\n<p><\/p>\n<p>&nbsp;<\/p>");
    $yzen_options['yzturbo'] = "disabled";
    $yzen_options['yzthumbnailturbo'] = "enabled";
    $yzen_options['yzselectthumbturbo'] = "medium";
    $yzen_options['yzad1'] = "disabled";
    $yzen_options['yzad1mesto'] = "После заголовка записи";
    $yzen_options['yzad1set'] = "РСЯ";
    $yzen_options['yzad1rsa'] = "";
    $yzen_options['yzad1fox1'] = "";
    $yzen_options['yzad1fox2'] = "";
    $yzen_options['yzad2'] = "disabled";
    $yzen_options['yzad2mesto'] = "В середине записи";
    $yzen_options['yzad2set'] = "РСЯ";
    $yzen_options['yzad2rsa'] = "";
    $yzen_options['yzad2fox1'] = "";
    $yzen_options['yzad2fox2'] = "";    
    $yzen_options['yzad3'] = "disabled";
    $yzen_options['yzad3mesto'] = "В конце записи";
    $yzen_options['yzad3set'] = "РСЯ";
    $yzen_options['yzad3rsa'] = "";
    $yzen_options['yzad3fox1'] = "";
    $yzen_options['yzad3fox2'] = "";    
    
    $yzen_options['yzrelated'] = "enabled";
    $yzen_options['yzrelatednumber'] = "5";
    $yzen_options['yzrelatedselectthumb'] = "thumbnail";
    
    $yzen_options['yzrazmer'] = "500";
    $yzen_options['yzremoveturbo'] = "disabled";
    
    $yzen_options['yzcounterselect'] = "Яндекс.Метрика";
    $yzen_options['yzmetrika'] = "";
    $yzen_options['yzliveinternet'] = "";
    $yzen_options['yzgoogle'] = "";
    $yzen_options['yzmailru'] = "";
    $yzen_options['yzrambler'] = "";
    $yzen_options['yzmediascope'] = "";
    
    $yzen_options['yzqueryselect'] = "Все таксономии, кроме исключенных";
    $yzen_options['yztaxlist'] = "";
    $yzen_options['yzaddtaxlist'] = "";
    

    add_option('yzen_options', $yzen_options);
    
    yzen_add_feed();
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_action('activate_rss-for-yandex-zen/rss-for-yandex-zen.php', 'yzen_init');
//функция установки значений по умолчанию при активации плагина end

//функция при деактивации плагина begin
function yzen_on_deactivation() {
	if ( ! current_user_can('activate_plugins') ) return;
    
    //удаляем ленту плагина при деактивации плагина и обновляем пермалинки begin
    $yzen_options = get_option('yzen_options'); 
    if (!isset($yzen_options['yzrssname'])) {$yzen_options['yzrssname']="zen";}
    global $wp_rewrite;
    if ( in_array( $yzen_options['yzrssname'], $wp_rewrite->feeds ) ) {
       unset($wp_rewrite->feeds[array_search($yzen_options['yzrssname'], $wp_rewrite->feeds)]);
    }
    $wp_rewrite->flush_rules();
    //удаляем ленту плагина при деактивации плагина и обновляем пермалинки end
}
register_deactivation_hook( __FILE__, 'yzen_on_deactivation' );
//функция при деактивации плагина end

//функция при удалении плагина begin
function yzen_on_uninstall() {
	if ( ! current_user_can('activate_plugins') ) return;
    delete_option('yzen_options');
}
register_uninstall_hook( __FILE__, 'yzen_on_uninstall' );
//функция при удалении плагина end

//загрузка файла локализации плагина begin
function yzen_setup(){
    load_plugin_textdomain('rss-for-yandex-zen');
}
add_action('init', 'yzen_setup');
//загрузка файла локализации плагина end

//добавление ссылки "Настройки" на странице со списком плагинов begin
function yzen_actions($links) {
	return array_merge(array('settings' => '<a href="options-general.php?page=rss-for-yandex-zen.php">' . __('Настройки', 'rss-for-yandex-zen') . '</a>'), $links);
}
add_filter('plugin_action_links_' . plugin_basename( __FILE__ ),'yzen_actions');
//добавление ссылки "Настройки" на странице со списком плагинов end

//функция загрузки скриптов и стилей плагина только в админке и только на странице настроек плагина begin
function yzen_files_admin($hook_suffix) {
	$purl = plugins_url('', __FILE__);

    if ( is_admin() && $hook_suffix == 'settings_page_rss-for-yandex-zen' ) {
    
    wp_register_script('yzen-lettering', $purl . '/inc/jquery.lettering.js');  
    wp_register_script('yzen-textillate', $purl . '/inc/jquery.textillate.js');  
	wp_register_style('yzen-animate', $purl . '/inc/animate.min.css');
    wp_register_script('yzen-script', $purl . '/inc/yzen-script.js', array(), '1.23');  
	
	if(!wp_script_is('jquery')) {wp_enqueue_script('jquery');}
    wp_enqueue_script('yzen-lettering');
    wp_enqueue_script('yzen-textillate');
    wp_enqueue_style('yzen-animate');
    wp_enqueue_script('yzen-script');
    
    }
}
add_action('admin_enqueue_scripts', 'yzen_files_admin');
//функция загрузки скриптов и стилей плагина только в админке и только на странице настроек плагина end

//функция вывода страницы настроек плагина begin
function yzen_options_page() {
$purl = plugins_url('', __FILE__);

if (isset($_POST['submit'])) {

//проверка безопасности при сохранении настроек плагина begin        
if ( ! wp_verify_nonce( $_POST['yzen_nonce'], plugin_basename(__FILE__) ) || ! current_user_can('edit_posts') ) {
   wp_die(__( 'Cheatin&#8217; uh?' ));
}
//проверка безопасности при сохранении настроек плагина end
    
    //проверяем и сохраняем введенные пользователем данные begin    
    $yzen_options = get_option('yzen_options');
    
    if (!preg_match('/[^A-Za-z0-9]/', $_POST['yzrssname']))  {
        $yzen_options['yzrssname'] = $_POST['yzrssname'];
        update_option('yzen_options', $yzen_options);
        yzen_add_feed();
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }
    
    $yzen_options['yzcategory'] = sanitize_text_field($_POST['yzcategory']);
    $yzen_options['yzrating'] = sanitize_text_field($_POST['yzrating']);
    $yzen_options['yztitle'] = sanitize_text_field($_POST['yztitle']);
    $yzen_options['yzlink'] = esc_url_raw($_POST['yzlink']);
    $yzen_options['yzdescription'] = sanitize_text_field($_POST['yzdescription']);
    $yzen_options['yzlanguage'] = sanitize_text_field($_POST['yzlanguage']);
    
    $yznumber = sanitize_text_field($_POST['yznumber']); 
    if (is_numeric($yznumber) && (int)$yznumber>=20) {
        $yzen_options['yznumber'] = sanitize_text_field($_POST['yznumber']);
    }
    
    $yzen_options['yztype'] = sanitize_text_field($_POST['yztype']);
    $yzen_options['yzfigcaption'] = sanitize_text_field($_POST['yzfigcaption']);
    $yzen_options['yzimgauthorselect'] = sanitize_text_field($_POST['yzimgauthorselect']);
    $yzen_options['yzimgauthor'] = sanitize_text_field($_POST['yzimgauthor']);
    $yzen_options['yzauthor'] = sanitize_text_field($_POST['yzauthor']);
    
    if(isset($_POST['yzthumbnail'])){$yzen_options['yzthumbnail'] = sanitize_text_field($_POST['yzthumbnail']);}else{$yzen_options['yzthumbnail'] = 'disabled';}
    $yzen_options['yzselectthumb'] = sanitize_text_field($_POST['yzselectthumb']);
    
    if(isset($_POST['yzseodesc'])){$yzen_options['yzseodesc'] = sanitize_text_field($_POST['yzseodesc']);}else{$yzen_options['yzseodesc'] = 'disabled';}
    $yzen_options['yzseoplugin'] = sanitize_text_field($_POST['yzseoplugin']);
    
    if(isset($_POST['yzexcludetags'])){$yzen_options['yzexcludetags'] = sanitize_text_field($_POST['yzexcludetags']);}else{$yzen_options['yzexcludetags'] = 'disabled';}
    $yzen_options['yzexcludetagslist'] = esc_textarea($_POST['yzexcludetagslist']);
    
    if(isset($_POST['yzexcludetags2'])){$yzen_options['yzexcludetags2'] = sanitize_text_field($_POST['yzexcludetags2']);}else{$yzen_options['yzexcludetags2'] = 'disabled';}
    $yzen_options['yzexcludetagslist2'] = esc_textarea($_POST['yzexcludetagslist2']);
    
    if(isset($_POST['yzexcludecontent'])){$yzen_options['yzexcludecontent'] = sanitize_text_field($_POST['yzexcludecontent']);}else{$yzen_options['yzexcludecontent'] = 'disabled';}
    $yzen_options['yzexcludecontentlist'] = addcslashes(esc_textarea($_POST['yzexcludecontentlist']), '/');
    
    if(isset($_POST['yzturbo'])){$yzen_options['yzturbo'] = sanitize_text_field($_POST['yzturbo']);}else{$yzen_options['yzturbo'] = 'disabled';}
    
    if(isset($_POST['yzthumbnailturbo'])){$yzen_options['yzthumbnailturbo'] = sanitize_text_field($_POST['yzthumbnailturbo']);}else{$yzen_options['yzthumbnailturbo'] = 'disabled';}
    $yzen_options['yzselectthumbturbo'] = sanitize_text_field($_POST['yzselectthumbturbo']);
    
    if(isset($_POST['yzad1'])){$yzen_options['yzad1'] = sanitize_text_field($_POST['yzad1']);}else{$yzen_options['yzad1'] = 'disabled';}
    $yzen_options['yzad1mesto'] = sanitize_text_field($_POST['yzad1mesto']);
    $yzen_options['yzad1set'] = sanitize_text_field($_POST['yzad1set']);
    $yzen_options['yzad1rsa'] = sanitize_text_field($_POST['yzad1rsa']);
    $yzen_options['yzad1fox1'] = sanitize_text_field($_POST['yzad1fox1']);
    $yzen_options['yzad1fox2'] = sanitize_text_field($_POST['yzad1fox2']);
    if(isset($_POST['yzad1'])) {
        if($yzen_options['yzad1set'] == "РСЯ" && !$yzen_options['yzad1rsa']) {$yzen_options['yzad1'] = 'disabled';}
        if($yzen_options['yzad1set'] == "ADFOX" && (!$yzen_options['yzad1fox1'] or !$yzen_options['yzad1fox2'])) {$yzen_options['yzad1'] = 'disabled';}
    }
    
    if(isset($_POST['yzad2'])){$yzen_options['yzad2'] = sanitize_text_field($_POST['yzad2']);}else{$yzen_options['yzad2'] = 'disabled';}
    $yzen_options['yzad2mesto'] = sanitize_text_field($_POST['yzad2mesto']);
    $yzen_options['yzad2set'] = sanitize_text_field($_POST['yzad2set']);
    $yzen_options['yzad2rsa'] = sanitize_text_field($_POST['yzad2rsa']);
    $yzen_options['yzad2fox1'] = sanitize_text_field($_POST['yzad2fox1']);
    $yzen_options['yzad2fox2'] = sanitize_text_field($_POST['yzad2fox2']);
    if(isset($_POST['yzad2'])) {
        if($yzen_options['yzad2set'] == "РСЯ" && !$yzen_options['yzad2rsa']) {$yzen_options['yzad2'] = 'disabled';}
        if($yzen_options['yzad2set'] == "ADFOX" && (!$yzen_options['yzad2fox1'] or !$yzen_options['yzad2fox2'])) {$yzen_options['yzad2'] = 'disabled';}
    }
    
    if(isset($_POST['yzad3'])){$yzen_options['yzad3'] = sanitize_text_field($_POST['yzad3']);}else{$yzen_options['yzad3'] = 'disabled';}
    $yzen_options['yzad3mesto'] = sanitize_text_field($_POST['yzad3mesto']);
    $yzen_options['yzad3set'] = sanitize_text_field($_POST['yzad3set']);
    $yzen_options['yzad3rsa'] = sanitize_text_field($_POST['yzad3rsa']);
    $yzen_options['yzad3fox1'] = sanitize_text_field($_POST['yzad3fox1']);
    $yzen_options['yzad3fox2'] = sanitize_text_field($_POST['yzad3fox2']);
    if(isset($_POST['yzad3'])) {
        if($yzen_options['yzad3set'] == "РСЯ" && !$yzen_options['yzad3rsa']) {$yzen_options['yzad3'] = 'disabled';}
        if($yzen_options['yzad3set'] == "ADFOX" && (!$yzen_options['yzad3fox1'] or !$yzen_options['yzad3fox2'])) {$yzen_options['yzad3'] = 'disabled';}
    }   
    
    if(isset($_POST['yzrelated'])){$yzen_options['yzrelated'] = sanitize_text_field($_POST['yzrelated']);}else{$yzen_options['yzrelated'] = 'disabled';}
    $yzrelatednumber = sanitize_text_field($_POST['yzrelatednumber']); 
    if (is_numeric($yzrelatednumber)) {
        $yzen_options['yzrelatednumber'] = sanitize_text_field($_POST['yzrelatednumber']);
    }
    $yzen_options['yzrelatedselectthumb'] = sanitize_text_field($_POST['yzrelatedselectthumb']);

    $yzrazmer = sanitize_text_field($_POST['yzrazmer']); 
    if (is_numeric($yzrazmer)) {
        $yzen_options['yzrazmer'] = sanitize_text_field($_POST['yzrazmer']);
    }    
    if(isset($_POST['yzremoveturbo'])){$yzen_options['yzremoveturbo'] = sanitize_text_field($_POST['yzremoveturbo']);}else{$yzen_options['yzremoveturbo'] = 'disabled';}
    
    $yzen_options['yzcounterselect'] = sanitize_text_field($_POST['yzcounterselect']);
    $yzen_options['yzmetrika'] = sanitize_text_field($_POST['yzmetrika']);
    $yzen_options['yzliveinternet'] = sanitize_text_field($_POST['yzliveinternet']);
    $yzen_options['yzgoogle'] = sanitize_text_field($_POST['yzgoogle']);
    $yzen_options['yzmailru'] = sanitize_text_field($_POST['yzmailru']);
    $yzen_options['yzrambler'] = sanitize_text_field($_POST['yzrambler']);
    $yzen_options['yzmediascope'] = sanitize_text_field($_POST['yzmediascope']);
    
    $yzen_options['yzqueryselect'] = sanitize_text_field($_POST['yzqueryselect']);
    $yzen_options['yztaxlist'] = esc_textarea($_POST['yztaxlist']);
    $yzen_options['yzaddtaxlist'] = esc_textarea($_POST['yzaddtaxlist']);
    

    update_option('yzen_options', $yzen_options);
    //проверяем и сохраняем введенные пользователем данные end
}
//установка новых опций при обновлении плагина у пользователей begin
$yzen_options = get_option('yzen_options');
if (!isset($yzen_options['yzthumbnail'])) {$yzen_options['yzthumbnail']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzselectthumb'])) {$yzen_options['yzselectthumb']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzseodesc'])) {$yzen_options['yzseodesc']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzseoplugin'])) {$yzen_options['yzseoplugin']="Yoast SEO";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzexcludetags'])) {$yzen_options['yzexcludetags']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzexcludetagslist'])) {$yzen_options['yzexcludetagslist']="<div>";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzexcludetags2'])) {$yzen_options['yzexcludetags2']="enabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzexcludetagslist2'])) {$yzen_options['yzexcludetagslist2']="<iframe>,<script>,<ins>,<style>,<object>";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzexcludecontent'])) {$yzen_options['yzexcludecontent']="enabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzexcludecontentlist'])) {$yzen_options['yzexcludecontentlist']=esc_textarea("<!--more-->\n<p><\/p>\n<p>&nbsp;<\/p>");update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzturbo'])) {$yzen_options['yzturbo']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzmetrika'])) {$yzen_options['yzmetrika']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad1'])) {$yzen_options['yzad1']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad1mesto'])) {$yzen_options['yzad1mesto']="После заголовка записи";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad1set'])) {$yzen_options['yzad1set']="РСЯ";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad1rsa'])) {$yzen_options['yzad1rsa']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad1fox1'])) {$yzen_options['yzad1fox1']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad1fox2'])) {$yzen_options['yzad1fox2']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad2'])) {$yzen_options['yzad2']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad2mesto'])) {$yzen_options['yzad2mesto']="В конце записи";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad2set'])) {$yzen_options['yzad2set']="РСЯ";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad2rsa'])) {$yzen_options['yzad2rsa']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad2fox1'])) {$yzen_options['yzad2fox1']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad2fox2'])) {$yzen_options['yzad2fox2']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzimgauthorselect'])) {$yzen_options['yzimgauthorselect']="Указать автора";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzrssname'])) {$yzen_options['yzrssname']="zen";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzthumbnailturbo'])) {$yzen_options['yzthumbnailturbo']="enabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzselectthumbturbo'])) {$yzen_options['yzselectthumbturbo']="medium";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad3'])) {$yzen_options['yzad3']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad3mesto'])) {$yzen_options['yzad3mesto']="После заголовка записи";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad3set'])) {$yzen_options['yzad3set']="РСЯ";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad3rsa'])) {$yzen_options['yzad3rsa']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad3fox1'])) {$yzen_options['yzad3fox1']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzad3fox2'])) {$yzen_options['yzad3fox2']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzrazmer'])) {$yzen_options['yzrazmer']="500";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzremoveturbo'])) {$yzen_options['yzremoveturbo']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzrelated'])) {$yzen_options['yzrelated']="disabled";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzrelatednumber'])) {$yzen_options['yzrelatednumber']="5";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzrelatedselectthumb'])) {$yzen_options['yzrelatedselectthumb']="medium";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzcounterselect'])) {$yzen_options['yzcounterselect']="Яндекс.Метрика";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzliveinternet'])) {$yzen_options['yzliveinternet']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzgoogle'])) {$yzen_options['yzgoogle']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzmailru'])) {$yzen_options['yzmailru']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzrambler'])) {$yzen_options['yzrambler']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzmediascope'])) {$yzen_options['yzmediascope']="";update_option('yzen_options', $yzen_options);}    
if (!isset($yzen_options['yzqueryselect'])) {$yzen_options['yzqueryselect']="Все таксономии, кроме исключенных";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yztaxlist'])) {$yzen_options['yztaxlist']="";update_option('yzen_options', $yzen_options);}
if (!isset($yzen_options['yzaddtaxlist'])) {$yzen_options['yzaddtaxlist']="";update_option('yzen_options', $yzen_options);}
//установка новых опций при обновлении плагина у пользователей end
$yzen_options = get_option('yzen_options');
?>
<?php   if (!empty($_POST) ) :
if ( ! wp_verify_nonce( $_POST['yzen_nonce'], plugin_basename(__FILE__) ) || ! current_user_can('edit_posts') ) {
   wp_die(__( 'Cheatin&#8217; uh?' ));
}
?>
<div id="message" class="updated fade"><p><strong><?php _e('Настройки сохранены.', 'rss-for-yandex-zen') ?></strong></p></div>
<?php endif; ?>

<div class="wrap">
<h2><?php _e('Настройки плагина &#171;Яндекс.Дзен&#187;', 'rss-for-yandex-zen'); ?></h2>

<div class="metabox-holder" id="poststuff">
<div class="meta-box-sortables">

<div class="postbox">

    <h3 style="border-bottom: 1px solid #EEE;background: #f7f7f7;"><span class="tcode"><?php _e("Вам нравится этот плагин ?", 'rss-for-yandex-zen'); ?></span></h3>
    <div class="inside" style="display: block;margin-right: 12px;">
        <img src="<?php echo $purl . '/img/icon_coffee.png'; ?>" title="<?php _e("Купить мне чашку кофе :)", 'rss-for-yandex-zen'); ?>" style=" margin: 5px; float:left;" />
		
        <p><?php _e("Привет, меня зовут <strong>Flector</strong>.", 'rss-for-yandex-zen'); ?></p>
        <p><?php _e("Я потратил много времени на разработку этого плагина.", 'rss-for-yandex-zen'); ?> <br />
		<?php _e("Поэтому не откажусь от небольшого пожертвования :)", 'rss-for-yandex-zen'); ?></p>
      <iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/donate.xml?account=41001443750704&quickpay=donate&payment-type-choice=on&mobile-payment-type-choice=on&default-sum=200&targets=%D0%9D%D0%B0+%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D1%83+WordPress-%D0%BF%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD%D0%BE%D0%B2.&project-name=&project-site=&button-text=05&successURL=" width="508" height="64"></iframe>
      
      <p><?php _e("Или вы можете заказать у меня услуги по WordPress, от мелких правок до создания полноценного сайта.", 'rss-for-yandex-zen'); ?><br />
        <?php _e("Быстро, качественно и дешево. Прайс-лист смотрите по адресу <a target='new' href='https://www.wpuslugi.ru/?from=yzen-plugin'>https://www.wpuslugi.ru/</a>.", 'rss-for-yandex-zen'); ?></p>
        <div style="clear:both;"></div>
    </div>
</div>

<form action="" method="post">

<div class="postbox">

    <h3 style="border-bottom: 1px solid #EEE;background: #f7f7f7;"><span class="tcode"><?php _e("Настройки", 'rss-for-yandex-zen'); ?></span></h3>
    <div class="inside" style="display: block;">

        <table class="form-table">
        
        <?php if ( get_option('permalink_structure') ) {
            $kor = get_bloginfo("url") .'/feed/' . '<strong>' . $yzen_options['yzrssname'] . '</strong>/';
            $rssname = get_bloginfo("url") .'/feed/' . $yzen_options['yzrssname'] . '/';
            echo '<p>Ваша RSS-лента для Яндекс.Дзена доступна по адресу: <a target="new" href="'.$rssname.'">'.$rssname.'</a><br />
            Для добавления ее в сервис Яндекс.Дзена заполните форму обратной связи на этой <a target="new" href="https://yandex.ru/support/zen/publishers/rss-connect.html#connection">странице</a>.</p>';
         } else {
            $kor = get_bloginfo("url") .'/?feed=' . '<strong>' . $yzen_options['yzrssname']. '</strong>';
            $rssname = get_bloginfo("url") .'/?feed=' . $yzen_options['yzrssname'] ;
            echo '<p>Ваша RSS-лента для Яндекс.Дзена доступна по адресу: <a target="new" href="'.$rssname.'">'.$rssname.'</a><br />
            Для добавления ее в сервис Яндекс.Дзена заполните форму обратной связи на этой <a target="new" href="https://yandex.ru/support/zen/publishers/rss-connect.html#connection">странице</a>.</p>'; 
         } ?>
        
            <tr>
                <th><?php _e("Имя RSS-ленты:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzrssname" size="40" value="<?php echo $yzen_options['yzrssname']; ?>" />
                    <br /><small><?php _e("Текущий URL RSS-ленты:", "rss-for-yandex-zen"); ?> <tt><?php echo $kor; ?></tt><br />
                    <?php _e("Только буквы и цифры, не меняйте без необходимости.", "rss-for-yandex-zen"); ?>
                    </small><div style="margin-bottom:20px;"></div>
                </td>
            </tr>
            <tr>
                <th><?php _e("Заголовок:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yztitle" size="40" value="<?php echo stripslashes($yzen_options['yztitle']); ?>" />
                    <br /><small><?php _e("Название издания.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr>
                <th><?php _e("Ссылка:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzlink" size="40" value="<?php echo stripslashes($yzen_options['yzlink']); ?>" />
                    <br /><small><?php _e("Адрес сайта издания.", "rss-for-yandex-zen"); ?> </small>
               </td>
            </tr>
            <tr>
                <th><?php _e("Описание:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzdescription" size="40" value="<?php echo stripslashes($yzen_options['yzdescription']); ?>" />
                    <br /><small><?php _e("Описание издания.", "rss-for-yandex-zen"); ?> </small>
               </td>
            </tr>
            <tr>
                <th><?php _e("Язык:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzlanguage" size="2" value="<?php echo stripslashes($yzen_options['yzlanguage']); ?>" />
                    <br /><small><?php _e("Язык статей издания в стандарте <a target='new' href='https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%B4%D1%8B_%D1%8F%D0%B7%D1%8B%D0%BA%D0%BE%D0%B2'>ISO 639-1</a> (Россия - <strong>ru</strong>, Украина - <strong>uk</strong> и т.д.)", "rss-for-yandex-zen"); ?> </small>
                    <div  style="margin-bottom:20px;"></div>
               </td>
            </tr>
           <tr>
                <th><?php _e("Количество записей:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yznumber" size="2" value="<?php echo stripslashes($yzen_options['yznumber']); ?>" />
                    <br /><small><?php _e("Количество записей в ленте (по требованиям Яндекса минимально необходимо <strong>20</strong> записей).", "rss-for-yandex-zen"); ?> </small>
               </td>
            </tr>
           <tr>
                <th><?php _e("Типы записей:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yztype" size="20" value="<?php echo stripslashes($yzen_options['yztype']); ?>" />
                    <br /><small><?php _e("Типы записей в ленте через запятую (<strong>post</strong> - записи, <strong>page</strong> - страницы и т.д.).<br />У произвольных типов записей должно быть поле <strong>post_content</strong>!", "rss-for-yandex-zen"); ?> </small>
               </td>
            </tr>
            <tr>
                <th><?php _e("Автор записей:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzauthor" size="20" value="<?php echo stripslashes($yzen_options['yzauthor']); ?>" />
                    <br /><small><?php _e("Автор записей (если не заполнено, то будет использовано имя автора записи).", "rss-for-yandex-zen"); ?> </small>
               </td>
            </tr>
            <tr>
                <th><?php _e("Описания изображений:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzfigcaption" id="capalt" style="width: 250px;">
                        <option value="Использовать alt по возможности" <?php if ($yzen_options['yzfigcaption'] == 'Использовать alt по возможности') echo "selected='selected'" ?>><?php _e("Использовать alt по возможности", "rss-for-yandex-zen"); ?></option>
                        <option value="Использовать название записи" <?php if ($yzen_options['yzfigcaption'] == 'Использовать название записи') echo "selected='selected'" ?>><?php _e("Использовать название записи", "rss-for-yandex-zen"); ?></option>
                        <option value="Отключить описания" <?php if ($yzen_options['yzfigcaption'] == 'Отключить описания') echo "selected='selected'" ?>><?php _e("Отключить описания", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Разметка \"описания\" для изображений (<tt>&lt;figcaption>Описание&lt;/figcaption></tt>).", "rss-for-yandex-zen"); ?> <br />
                    <span id="altimg"><?php _e("В случае отсутствия у изображения alt-атрибута для описания изображения будет использовано название записи.", "rss-for-yandex-zen"); ?> </span></small>
                </td>
            </tr>
            <tr>
                <th><?php _e("Автор изображений:", "rss-for-yandex-zen") ?></th>
                <td>
                    <select name="yzimgauthorselect" id="imgselect" style="width: 250px;">
                        <option value="Автор записи" <?php if ($yzen_options['yzimgauthorselect'] == 'Автор записи') echo "selected='selected'" ?>><?php _e("Автор записи", "rss-for-yandex-zen"); ?></option>
                        <option value="Указать автора" <?php if ($yzen_options['yzimgauthorselect'] == 'Указать автора') echo "selected='selected'" ?>><?php _e("Указать автора", "rss-for-yandex-zen"); ?></option>
                        <option value="Отключить указание автора" <?php if ($yzen_options['yzimgauthorselect'] == 'Отключить указание автора') echo "selected='selected'" ?>><?php _e("Отключить указание автора", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Разметка \"автора\" для изображений (<tt>&lt;span class=\"copyright\">Автор&lt;/span></tt>).", "rss-for-yandex-zen"); ?> <br />
                    
                    </small>
               </td>
            </tr>
            <tr id="ownname" style="display:none;">
                <th><?php _e("Имя автора изображений:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzimgauthor" size="20" value="<?php echo stripslashes($yzen_options['yzimgauthor']); ?>" />
                    <br /><small><?php _e("Автор изображений (если не заполнено, то будет использовано имя автора записи).", "rss-for-yandex-zen"); ?> </small>
               </td>
            </tr>
            <tr>
                <th><?php _e("Тематика записей по умолчанию:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzcategory" style="width: 250px;">
                        <option value="Происшествия" <?php if ($yzen_options['yzcategory'] == 'Происшествия') echo "selected='selected'" ?>><?php _e("Происшествия", "rss-for-yandex-zen"); ?></option>
                        <option value="Политика" <?php if ($yzen_options['yzcategory'] == 'Политика') echo "selected='selected'" ?>><?php _e("Политика", "rss-for-yandex-zen"); ?></option>
                        <option value="Война" <?php if ($yzen_options['yzcategory'] == 'Война') echo "selected='selected'" ?>><?php _e("Война", "rss-for-yandex-zen"); ?></option>
                        <option value="Общество" <?php if ($yzen_options['yzcategory'] == 'Общество') echo "selected='selected'" ?>><?php _e("Общество", "rss-for-yandex-zen"); ?></option>
                        <option value="Экономика" <?php if ($yzen_options['yzcategory'] == 'Экономика') echo "selected='selected'" ?>><?php _e("Экономика", "rss-for-yandex-zen"); ?></option>
                        <option value="Спорт" <?php if ($yzen_options['yzcategory'] == 'Спорт') echo "selected='selected'" ?>><?php _e("Спорт", "rss-for-yandex-zen"); ?></option>
                        <option value="Технологии" <?php if ($yzen_options['yzcategory'] == 'Технологии') echo "selected='selected'" ?>><?php _e("Технологии", "rss-for-yandex-zen"); ?></option>
                        <option value="Наука" <?php if ($yzen_options['yzcategory'] == 'Наука') echo "selected='selected'" ?>><?php _e("Наука", "rss-for-yandex-zen"); ?></option>
                        <option value="Игры" <?php if ($yzen_options['yzcategory'] == 'Игры') echo "selected='selected'" ?>><?php _e("Игры", "rss-for-yandex-zen"); ?></option>
                        <option value="Музыка" <?php if ($yzen_options['yzcategory'] == 'Музыка') echo "selected='selected'" ?>><?php _e("Музыка", "rss-for-yandex-zen"); ?></option>
                        <option value="Литература" <?php if ($yzen_options['yzcategory'] == 'Литература') echo "selected='selected'" ?>><?php _e("Литература", "rss-for-yandex-zen"); ?></option>
                        <option value="Кино" <?php if ($yzen_options['yzcategory'] == 'Кино') echo "selected='selected'" ?>><?php _e("Кино", "rss-for-yandex-zen"); ?></option>
                        <option value="Культура" <?php if ($yzen_options['yzcategory'] == 'Культура') echo "selected='selected'" ?>><?php _e("Культура", "rss-for-yandex-zen"); ?></option>
                        <option value="Мода" <?php if ($yzen_options['yzcategory'] == 'Мода') echo "selected='selected'" ?>><?php _e("Мода", "rss-for-yandex-zen"); ?></option>
                        <option value="Знаменитости" <?php if ($yzen_options['yzcategory'] == 'Знаменитости') echo "selected='selected'" ?>><?php _e("Знаменитости", "rss-for-yandex-zen"); ?></option>
                        <option value="Психология" <?php if ($yzen_options['yzcategory'] == 'Психология') echo "selected='selected'" ?>><?php _e("Психология", "rss-for-yandex-zen"); ?></option>
                        <option value="Здоровье" <?php if ($yzen_options['yzcategory'] == 'Здоровье') echo "selected='selected'" ?>><?php _e("Здоровье", "rss-for-yandex-zen"); ?></option>
                        <option value="Авто" <?php if ($yzen_options['yzcategory'] == 'Авто') echo "selected='selected'" ?>><?php _e("Авто", "rss-for-yandex-zen"); ?></option>
                        <option value="Дом" <?php if ($yzen_options['yzcategory'] == 'Дом') echo "selected='selected'" ?>><?php _e("Дом", "rss-for-yandex-zen"); ?></option>
                        <option value="Хобби" <?php if ($yzen_options['yzcategory'] == 'Хобби') echo "selected='selected'" ?>><?php _e("Хобби", "rss-for-yandex-zen"); ?></option>
                        <option value="Еда" <?php if ($yzen_options['yzcategory'] == 'Еда') echo "selected='selected'" ?>><?php _e("Еда", "rss-for-yandex-zen"); ?></option>
                        <option value="Дизайн" <?php if ($yzen_options['yzcategory'] == 'Дизайн') echo "selected='selected'" ?>><?php _e("Дизайн", "rss-for-yandex-zen"); ?></option>
                        <option value="Фотографии" <?php if ($yzen_options['yzcategory'] == 'Фотографии') echo "selected='selected'" ?>><?php _e("Фотографии", "rss-for-yandex-zen"); ?></option>
                        <option value="Юмор" <?php if ($yzen_options['yzcategory'] == 'Юмор') echo "selected='selected'" ?>><?php _e("Юмор", "rss-for-yandex-zen"); ?></option>
                        <option value="Природа" <?php if ($yzen_options['yzcategory'] == 'Природа') echo "selected='selected'" ?>><?php _e("Природа", "rss-for-yandex-zen"); ?></option>
                        <option value="Путешествия" <?php if ($yzen_options['yzcategory'] == 'Путешествия') echo "selected='selected'" ?>><?php _e("Путешествия", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Тематика по умолчанию (если при публикации записи не задана конкретная тематика, то будет использована тематика по умолчанию).", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr>
                <th><?php _e("Контент для взрослых по умолчанию:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzrating" style="width: 250px;">
                        <option value="Да (для взрослых)" <?php if ($yzen_options['yzrating'] == 'Да (для взрослых)') echo "selected='selected'" ?>><?php _e("Да (для взрослых)", "rss-for-yandex-zen"); ?></option>
                        <option value="Нет (не для взрослых)" <?php if ($yzen_options['yzrating'] == 'Нет (не для взрослых)') echo "selected='selected'" ?>><?php _e("Нет (не для взрослых)", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Если при публикации записи не выбрана эта опция, то будет использовано значение по умолчанию. Учтите, что в понимании Яндекса контент не для взрослых подразумевает записи, которые можно показывать подросткам от <strong>13</strong> лет.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            
            <tr>
                <th></th>
                <td>
                    <input type="submit" name="submit" class="button button-primary" value="<?php _e('Сохранить настройки &raquo;', 'rss-for-yandex-zen'); ?>" />
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="postbox">
    <h3 style="border-bottom: 1px solid #EEE;background: #f7f7f7;"><span class="tcode"><?php _e('Продвинутые настройки', 'rss-for-yandex-zen'); ?></span></h3>
	  <div class="inside" style="padding-bottom:15px;display: block;">
     
        <table class="form-table">
        
        <p><?php _e("В данной секции находятся продвинутые настройки. <br />Пожалуйста, будьте внимательны в этом разделе!", "rss-for-yandex-zen"); ?> </p>
        
        
            <tr class="yzqueryselect">
                <th><?php _e("Включить в RSS:", "rss-for-yandex-zen") ?></th>
                <td>
                    <select name="yzqueryselect" id="yzqueryselect" style="width: 280px;">
                        <option value="Все таксономии, кроме исключенных" <?php if ($yzen_options['yzqueryselect'] == 'Все таксономии, кроме исключенных') echo "selected='selected'" ?>><?php _e("Все таксономии, кроме исключенных", "rss-for-yandex-zen"); ?></option>
                        <option value="Только указанные таксономии" <?php if ($yzen_options['yzqueryselect'] == 'Только указанные таксономии') echo "selected='selected'" ?>><?php _e("Только указанные таксономии", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Внимание! Будьте осторожны с этой настройкой!", "rss-for-yandex-zen"); ?> <br />
                    <span id="includespan"><?php _e("Обязательно установите ниже таксономии для включения в ленту - иначе лента будет пустая.", "rss-for-yandex-zen"); ?> <br /></span>
                    <span id="excludespan"><?php _e("По умолчанию в ленту попадают записи всех таксономий, кроме указанных ниже.", "rss-for-yandex-zen"); ?> <br /></span>
                    </small>
               </td>
            </tr> 
            <tr class="yztaxlisttr">
                <th><?php _e("Таксономии для исключения:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <textarea rows="3" cols="60" name="yztaxlist" id="yztaxlist"><?php echo stripslashes($yzen_options['yztaxlist']); ?></textarea>
                    <br /><small><?php _e("Используемый формат: <strong>taxonomy_name:id1,id2,id3</strong>", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Пример: <code>category:1,2,4</code> - записи рубрик с ID равным 1, 2 и 4 будут <strong style='color:red;'>исключены</strong> из RSS-ленты.", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Каждая новая таксономия должна начинаться с новой строки.", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Стандартные таксономии WordPress: рубрика: <code>category</code>, метка: <code>post_tag</code>.", "rss-for-yandex-zen"); ?>
                    </small>
                </td>
            </tr>
            <tr class="yzaddtaxlisttr">
                <th><?php _e("Таксономии для добавления:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <textarea rows="3" cols="60" name="yzaddtaxlist" id="yzaddtaxlist"><?php echo stripslashes($yzen_options['yzaddtaxlist']); ?></textarea>
                    <br /><small><?php _e("Используемый формат: <strong>taxonomy_name:id1,id2,id3</strong>", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Пример: <code>category:1,2,4</code> - записи рубрик с ID равным 1, 2 и 4 будут <strong style='color:red;'>добавлены</strong> в RSS-ленту.", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Каждая новая таксономия должна начинаться с новой строки.", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Стандартные таксономии WordPress: рубрика: <code>category</code>, метка: <code>post_tag</code>.", "rss-for-yandex-zen"); ?>
                    </small>
                </td>
            </tr>    
            <tr class="yzthumbnailtr">
                <th><?php _e("Миниатюры в RSS:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzthumbnail"><input type="checkbox" value="enabled" name="yzthumbnail" id="yzthumbnail" <?php if ($yzen_options['yzthumbnail'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Добавить миниатюру к записи", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("В начало записи в RSS будет добавлена миниатюра записи (изображение записи).", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Эта опция касается только обычных записей (для турбо-записей нужно включить отдельную опцию).", "rss-for-yandex-zen"); ?>
                    </small>
                </td>
            </tr>
            <tr class="yzselectthumbtr" style="display:none;">
                <th><?php _e("Размер миниатюры в RSS:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <select name="yzselectthumb" style="width: 250px;">
                        <?php $image_sizes = get_intermediate_image_sizes(); ?>
                        <?php foreach ($image_sizes as $size_name): ?>
                            <option value="<?php echo $size_name ?>" <?php if ($yzen_options['yzselectthumb'] == $size_name) echo "selected='selected'" ?>><?php echo $size_name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <br /><small><?php _e("Выберите нужный размер миниатюры (в списке находятся все зарегистрированные на сайте размеры миниатюр). ", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzseodesctr">
                <th><?php _e("Описания записей:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzseodesc"><input type="checkbox" value="enabled" name="yzseodesc" id="yzseodesc" <?php if ($yzen_options['yzseodesc'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Использовать данные из SEO-плагинов", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("В качестве описания записи (rss-тег <tt>&lt;description&gt;</tt>) будет использовано описание записи из выбранного SEO-плагина.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzseoplugintr" style="display:none;">
                <th><?php _e("SEO-плагин:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <select name="yzseoplugin" style="width: 250px;">
                        <option value="Yoast SEO" <?php if ($yzen_options['yzseoplugin'] == 'Yoast SEO') echo "selected='selected'" ?>><?php _e("Yoast SEO", "rss-for-yandex-zen"); ?></option>
                        <option value="All in One SEO Pack" <?php if ($yzen_options['yzseoplugin'] == 'All in One SEO Pack') echo "selected='selected'" ?>><?php _e("All in One SEO Pack", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Выберите используемый вами SEO-плагин. <br /> Если описание записи в SEO-плагине не установлено, то будет использовано стандартное описание записи (автогенерированное из первых 55 слов записи).", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzexcludetagstr">
                <th><?php _e("Фильтр тегов (без контента):", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzexcludetags"><input type="checkbox" value="enabled" name="yzexcludetags" id="yzexcludetags" <?php if ($yzen_options['yzexcludetags'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Удалить указанные html-теги", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Из контента записей будут удалены все указанные html-теги (<strong>сам контент этих тегов останется</strong>).", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzexcludetagslisttr">
                <th><?php _e("Теги для удаления:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <textarea rows="3" cols="60" name="yzexcludetagslist" id="yzexcludetagslist"><?php echo stripslashes($yzen_options['yzexcludetagslist']); ?></textarea>
                    <br /><small><?php _e("Список удаляемых html-тегов через запятую.", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Указывать классы, идентификаторы и прочее не требуется.", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Самозакрывающиеся теги вроде <tt>&lt;img src=\"...\" /></tt> и <tt>&lt;br /></tt> удалить нельзя.", "rss-for-yandex-zen"); ?><br />
                    </small>
                </td>
            </tr>
            <tr class="yzexcludetags2tr">
                <th><?php _e("Фильтр тегов (с контентом):", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzexcludetags2"><input type="checkbox" value="enabled" name="yzexcludetags2" id="yzexcludetags2" <?php if ($yzen_options['yzexcludetags2'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Удалить указанные html-теги", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Из контента записей будут удалены все указанные html-теги (<strong>включая сам контент этих тегов</strong>).", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzexcludetagslist2tr">
                <th><?php _e("Теги для удаления:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <textarea rows="3" cols="60" name="yzexcludetagslist2" id="yzexcludetagslist2"><?php echo stripslashes($yzen_options['yzexcludetagslist2']); ?></textarea>
                    <br /><small><?php _e("Список удаляемых html-тегов через запятую.", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Указывать классы, идентификаторы и прочее не требуется.", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("По умолчанию в список включены все теги, о которых точно известно, что они не нравятся тех. поддержке Яндекс.Дзена.", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Самозакрывающиеся теги вроде <tt>&lt;img src=\"...\" /></tt> и <tt>&lt;br /></tt> удалить нельзя.", "rss-for-yandex-zen"); ?><br />
                    </small>
                </td>
            </tr>
            <tr class="yzexcludecontenttr">
                <th><?php _e("Контент для удаления:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzexcludecontent"><input type="checkbox" value="enabled" name="yzexcludecontent" id="yzexcludecontent" <?php if ($yzen_options['yzexcludecontent'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Удалить указанный контент из RSS", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Точные вхождения указанного контента будут удалены из записей в RSS-ленте.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzexcludecontentlisttr">
                <th><?php _e("Список удаляемого контента:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <textarea rows="5" cols="60" name="yzexcludecontentlist" id="yzexcludecontentlist"><?php echo stripcslashes($yzen_options['yzexcludecontentlist']); ?></textarea>
                    <br /><small><?php _e("Каждый новый шаблон для удаления должен начинаться с новой строки.", "rss-for-yandex-zen"); ?> <br />
                    </small>
                </td>
            </tr>

            <tr>
                <th></th>
                <td>
                    <input type="submit" name="submit" class="button button-primary" value="<?php _e('Сохранить настройки &raquo;', 'rss-for-yandex-zen'); ?>" />
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="postbox">
    <h3 style="border-bottom: 1px solid #EEE;background: #f7f7f7;"><span class="tcode"><?php _e('Настройки Яндекс.Турбо', 'rss-for-yandex-zen'); ?></span></h3>
	  <div class="inside" style="padding-bottom:15px;display: block;">
     
        <table class="form-table">
        
        <p><?php _e("Крайне не рекомендую использовать опцию включения поддержки Яндекс.Турбо именно в этом плагине. <br /> Используйте отдельный специализированный плагин <a target='new' href='https://ru.wordpress.org/plugins/rss-for-yandex-turbo/'>RSS for Yandex Turbo</a>. ", "rss-for-yandex-zen"); ?> <br /> </p>
        
            <tr class="yzturbotr">
                <th><?php _e("Яндекс.Турбо:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzturbo"><input type="checkbox" value="enabled" name="yzturbo" id="yzturbo" <?php if ($yzen_options['yzturbo'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Включить поддержку Яндекс.Турбо для записей в RSS-ленте", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Будет добавлена необходимая разметка к записям RSS-ленты.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="counters myturbo" style="display:none;">
                <th><?php _e("Счетчики:", "rss-for-yandex-zen") ?></th>
                <td>
                    <select name="yzcounterselect" id="yzcounterselect" style="width: 250px;">
                        <option value="Яндекс.Метрика" <?php if ($yzen_options['yzcounterselect'] == 'Яндекс.Метрика') echo "selected='selected'" ?>><?php _e("Яндекс.Метрика", "rss-for-yandex-zen"); ?></option>
                        <option value="LiveInternet" <?php if ($yzen_options['yzcounterselect'] == 'LiveInternet') echo "selected='selected'" ?>><?php _e("LiveInternet", "rss-for-yandex-zen"); ?></option>
                        <option value="Google Analytics" <?php if ($yzen_options['yzcounterselect'] == 'Google Analytics') echo "selected='selected'" ?>><?php _e("Google Analytics", "rss-for-yandex-zen"); ?></option>
                        <option value="Рейтинг Mail.RU" <?php if ($yzen_options['yzcounterselect'] == 'Рейтинг Mail.RU') echo "selected='selected'" ?>><?php _e("Рейтинг Mail.RU", "rss-for-yandex-zen"); ?></option>
                        <option value="Rambler Топ-100" <?php if ($yzen_options['yzcounterselect'] == 'Rambler Топ-100') echo "selected='selected'" ?>><?php _e("Rambler Топ-100", "rss-for-yandex-zen"); ?></option>
                        <option value="Mediascope (TNS)" <?php if ($yzen_options['yzcounterselect'] == 'Mediascope (TNS)') echo "selected='selected'" ?>><?php _e("Mediascope (TNS)", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Выберите нужный счетчик и укажите его идентификатор. <br /> В ленте будут использованы все указанные вами счетчики.", "rss-for-yandex-zen"); ?> <br />
                    </small>
               </td>
            </tr>
            <tr class="metrika myturbo" style="display:none;">
                <th><?php _e("Яндекс.Метрика:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzmetrika" size="22" value="<?php echo stripslashes($yzen_options['yzmetrika']); ?>" />
                    <br /><small><?php _e("Укажите <strong>ID</strong> счетчика Яндекс.Метрики (например: <tt>33382498</tt>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-counter-id.html'>как узнать ID счетчика</a>).", "rss-for-yandex-zen"); ?> </small>
                    <?php if ($yzen_options['yzad1'] == 'enabled') echo '<div style="margin-top:30px"></div>'; ?>
               </td>
            </tr>
            <tr class="liveinternet myturbo" style="display:none;">
                <th><?php _e("LiveInternet:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzliveinternet" size="22" value="<?php echo stripslashes($yzen_options['yzliveinternet']); ?>" />
                    <br /><small><?php _e("Укажите <strong>ID</strong> счетчика LiveInternet (например: <tt>site.ru</tt>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-counter-id.html'>как узнать ID счетчика</a>).", "rss-for-yandex-zen"); ?> </small>
                    <?php if ($yzen_options['yzad1'] == 'enabled') echo '<div style="margin-top:30px"></div>'; ?>
               </td>
            </tr>
            <tr class="google myturbo" style="display:none;">
                <th><?php _e("Google Analytics:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzgoogle" size="22" value="<?php echo stripslashes($yzen_options['yzgoogle']); ?>" />
                    <br /><small><?php _e("Укажите <strong>ID</strong> счетчика Google Analytics (например: <tt>UA-12340005-6</tt>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-counter-id.html'>как узнать ID счетчика</a>).", "rss-for-yandex-zen"); ?> </small>
                    <?php if ($yzen_options['yzad1'] == 'enabled') echo '<div style="margin-top:30px"></div>'; ?>
               </td>
            </tr>
            <tr class="mailru myturbo" style="display:none;">
                <th><?php _e("Рейтинг Mail.RU:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzmailru" size="22" value="<?php echo stripslashes($yzen_options['yzmailru']); ?>" />
                    <br /><small><?php _e("Укажите <strong>ID</strong> счетчика Рейтинг Mail.RU (например: <tt>123456</tt>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-counter-id.html'>как узнать ID счетчика</a>).", "rss-for-yandex-zen"); ?> </small>
                    <?php if ($yzen_options['yzad1'] == 'enabled') echo '<div style="margin-top:30px"></div>'; ?>
               </td>
            </tr>
            <tr class="rambler myturbo" style="display:none;">
                <th><?php _e("Rambler Топ-100:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzrambler" size="22" value="<?php echo stripslashes($yzen_options['yzrambler']); ?>" />
                    <br /><small><?php _e("Укажите <strong>ID</strong> счетчика Rambler Топ-100 (например: <tt>4505046</tt>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-counter-id.html'>как узнать ID счетчика</a>).", "rss-for-yandex-zen"); ?> </small>
                    <?php if ($yzen_options['yzad1'] == 'enabled') echo '<div style="margin-top:30px"></div>'; ?>
               </td>
            </tr>
            <tr class="mediascope myturbo" style="display:none;">
                <th><?php _e("Mediascope (TNS):", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzmediascope" size="22" value="<?php echo stripslashes($yzen_options['yzmediascope']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор <strong>tmsec</strong> счетчика Mediascope (TNS) (<a target='new' href='https://yandex.ru/support/webmaster/turbo/find-counter-id.html'>как узнать ID счетчика</a>).", "rss-for-yandex-zen"); ?> </small>
                    <?php if ($yzen_options['yzad1'] == 'enabled') echo '<div style="margin-top:30px"></div>'; ?>
               </td>
            </tr>
            <tr class="yzthumbnailturbotr myturbo" style="display:none;">
                <th><?php _e("Миниатюры в RSS:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzthumbnailturbo"><input type="checkbox" value="enabled" name="yzthumbnailturbo" id="yzthumbnailturbo" <?php if ($yzen_options['yzthumbnailturbo'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Добавить миниатюру к заголовку записи", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("В заголовок записи в RSS (тег <tt>&lt;header></tt>) будет добавлена миниатюра записи (изображение записи).", "rss-for-yandex-zen"); ?> <br /><?php _e("Эта опция касается только турбо-записей (для обычных записей нужно включить отдельную опцию).", "rss-for-yandex-zen"); ?> 
                    
                    </small>
                </td>
            </tr>
            <tr class="yzselectthumbturbotr myturbo" style="display:none;">
                <th><?php _e("Размер миниатюры в RSS:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <select name="yzselectthumbturbo" style="width: 250px;">
                        <?php $image_sizes = get_intermediate_image_sizes(); ?>
                        <?php foreach ($image_sizes as $size_name): ?>
                            <option value="<?php echo $size_name ?>" <?php if ($yzen_options['yzselectthumbturbo'] == $size_name) echo "selected='selected'" ?>><?php echo $size_name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <br /><small><?php _e("Выберите нужный размер миниатюры (в списке находятся все зарегистрированные на сайте размеры миниатюр). ", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzrelatedtr myturbo" style="display:none;">
                <th><?php _e("Похожие записи:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzrelated"><input type="checkbox" value="enabled" name="yzrelated" id="yzrelated" <?php if ($yzen_options['yzrelated'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Выводить в RSS блок похожих записей", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("В RSS-ленту будет добавлен блок похожих записей (тег <tt>&lt;yandex:related></tt>).", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="yzrelatedchildtr myturbo" style="display:none;">
                <th><?php _e("Количество похожих записей:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzrelatednumber" size="2" value="<?php echo stripslashes($yzen_options['yzrelatednumber']); ?>" />
                    <br /><small><?php _e("Укажите число записей в блоке похожих записей.", "rss-for-yandex-zen"); ?> <br >
                    <?php _e("Список похожих записей будет формироваться случайным образом из записей рубрики текущей записи.", "rss-for-yandex-zen"); ?></small>
               </td>
            </tr>
            <tr class="yzrelatedchildtr myturbo" style="display:none;">
                <th><?php _e("Миниатюра для похожих записей:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <select name="yzrelatedselectthumb" style="width: 250px;">
                        <?php $image_sizes = get_intermediate_image_sizes(); ?>
                        <?php foreach ($image_sizes as $size_name): ?>
                            <option value="<?php echo $size_name ?>" <?php if ($yzen_options['yzrelatedselectthumb'] == $size_name) echo "selected='selected'"; ?>><?php echo $size_name ?></option>
                        <?php endforeach; ?>
                            <option value="Не использовать" <?php if ($yzen_options['yzrelatedselectthumb'] == 'Не использовать') echo "selected='selected'"; ?>><?php echo 'Не использовать'; ?></option>
                    </select>
                    <br /><small><?php _e("Выберите нужный размер миниатюры (в списке находятся все зарегистрированные на сайте размеры миниатюр). ", "rss-for-yandex-zen"); ?> <br /><?php _e("Вывод миниатюр для похожих записей можно отключить.", "rss-for-yandex-zen"); ?>
                    </small>
                </td>
            </tr>
            <tr class="myturbo" style="display:none;">
                <th><?php _e("Блок рекламы #1:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzad1"><input type="checkbox" value="enabled" name="yzad1" id="yzad1" <?php if ($yzen_options['yzad1'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Включить первый блок рекламы", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Будет включен блок рекламы на turbo-страницах в выбранном вами месте.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo block1" style="display:none;">
                <th><?php _e("Место блока рекламы #1:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzad1mesto" style="width: 200px;">
                        <option value="После заголовка записи" <?php if ($yzen_options['yzad1mesto'] == 'После заголовка записи') echo "selected='selected'" ?>><?php _e("После заголовка записи", "rss-for-yandex-zen"); ?></option>
                        <option value="В середине записи" <?php if ($yzen_options['yzad1mesto'] == 'В середине записи') echo "selected='selected'" ?>><?php _e("В середине записи", "rss-for-yandex-zen"); ?></option>
                        <option value="В конце записи" <?php if ($yzen_options['yzad1mesto'] == 'В конце записи') echo "selected='selected'" ?>><?php _e("В конце записи", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Место размещения блока рекламы #1 в записи.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo block1" style="display:none;">
                <th><?php _e("Рекламная сеть блока рекламы #1:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzad1set" id="yzad1set" style="width: 200px;">
                        <option value="РСЯ" <?php if ($yzen_options['yzad1set'] == 'РСЯ') echo "selected='selected'" ?>><?php _e("РСЯ", "rss-for-yandex-zen"); ?></option>
                        <option value="ADFOX" <?php if ($yzen_options['yzad1set'] == 'ADFOX') echo "selected='selected'" ?>><?php _e("ADFOX", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Рекламная сеть блока рекламы #1.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo trrsa block1" style="display:none;">
                <th><?php _e("РСЯ идентификатор:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad1rsa" size="22" value="<?php echo stripslashes($yzen_options['yzad1rsa']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор блока РСЯ (например, <strong>RA-123456-7</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
                    <div style="margin-top:30px;"></div>
               </td>
            </tr>
            <tr class="myturbo trfox1 block1" style="display:none;">
                <th><?php _e("ADFOX OwnerId:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad1fox1" size="22" value="<?php echo stripslashes($yzen_options['yzad1fox1']); ?>" />
                    <br /><small><?php _e("Укажите ownerId блока ADFOX (например, <strong>123456</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
               </td>
            </tr>
            <tr class="myturbo trfox2 block1" style="display:none;">
                <th><?php _e("ADFOX идентификатор:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad1fox2" size="22" value="<?php echo stripslashes($yzen_options['yzad1fox2']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор контейнера ADFOX (например, <strong>adfox</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
                    <div style="margin-top:30px;"></div>
               </td>
            </tr>
            <tr class="myturbo" style="display:none;">
                <th><?php _e("Блок рекламы #2:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzad2"><input type="checkbox" value="enabled" name="yzad2" id="yzad2" <?php if ($yzen_options['yzad2'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Включить второй блок рекламы", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Будет включен блок рекламы на turbo-страницах в выбранном вами месте.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo block2" style="display:none;">
                <th><?php _e("Место блока рекламы #2:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzad2mesto" style="width: 200px;">
                        <option value="После заголовка записи" <?php if ($yzen_options['yzad2mesto'] == 'После заголовка записи') echo "selected='selected'" ?>><?php _e("После заголовка записи", "rss-for-yandex-zen"); ?></option>
                        <option value="В середине записи" <?php if ($yzen_options['yzad2mesto'] == 'В середине записи') echo "selected='selected'" ?>><?php _e("В середине записи", "rss-for-yandex-zen"); ?></option>
                        <option value="В конце записи" <?php if ($yzen_options['yzad2mesto'] == 'В конце записи') echo "selected='selected'" ?>><?php _e("В конце записи", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Место размещения блока рекламы #2 в записи.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo block2" style="display:none;">
                <th><?php _e("Рекламная сеть блока рекламы #2:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzad2set" id="yzad2set" style="width: 200px;">
                        <option value="РСЯ" <?php if ($yzen_options['yzad2set'] == 'РСЯ') echo "selected='selected'" ?>><?php _e("РСЯ", "rss-for-yandex-zen"); ?></option>
                        <option value="ADFOX" <?php if ($yzen_options['yzad2set'] == 'ADFOX') echo "selected='selected'" ?>><?php _e("ADFOX", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Рекламная сеть блока рекламы #2.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo trrsa2 block2" style="display:none;">
                <th><?php _e("РСЯ идентификатор:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad2rsa" size="22" value="<?php echo stripslashes($yzen_options['yzad2rsa']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор блока РСЯ (например, <strong>RA-123456-7</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
               </td>
            </tr>
            <tr class="myturbo trfox12 block2" style="display:none;">
                <th><?php _e("ADFOX OwnerId:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad2fox1" size="22" value="<?php echo stripslashes($yzen_options['yzad2fox1']); ?>" />
                    <br /><small><?php _e("Укажите ownerId блока ADFOX (например, <strong>123456</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
               </td>
            </tr>
            <tr class="myturbo trfox22 block2" style="display:none;">
                <th><?php _e("ADFOX идентификатор:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad2fox2" size="22" value="<?php echo stripslashes($yzen_options['yzad2fox2']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор контейнера ADFOX (например, <strong>adfox</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
               </td>
            </tr>
            <tr class="myturbo">
                <th><?php _e("Блок рекламы #3:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzad3"><input type="checkbox" value="enabled" name="yzad3" id="yzad3" <?php if ($yzen_options['yzad3'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Включить третий блок рекламы", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Будет включен блок рекламы на turbo-страницах в выбранном вами месте.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo block3" style="display:none;">
                <th><?php _e("Место блока рекламы #3:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzad3mesto" style="width: 200px;">
                        <option value="После заголовка записи" <?php if ($yzen_options['yzad3mesto'] == 'После заголовка записи') echo "selected='selected'" ?>><?php _e("После заголовка записи", "rss-for-yandex-zen"); ?></option>
                        <option value="В середине записи" <?php if ($yzen_options['yzad3mesto'] == 'В середине записи') echo "selected='selected'" ?>><?php _e("В середине записи", "rss-for-yandex-zen"); ?></option>
                        <option value="В конце записи" <?php if ($yzen_options['yzad3mesto'] == 'В конце записи') echo "selected='selected'" ?>><?php _e("В конце записи", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Место размещения блока рекламы #3 в записи.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo block3" style="display:none;">
                <th><?php _e("Рекламная сеть блока рекламы #3:", 'rss-for-yandex-zen') ?></th>
                <td>
                     <select name="yzad3set" id="yzad3set" style="width: 200px;">
                        <option value="РСЯ" <?php if ($yzen_options['yzad3set'] == 'РСЯ') echo "selected='selected'" ?>><?php _e("РСЯ", "rss-for-yandex-zen"); ?></option>
                        <option value="ADFOX" <?php if ($yzen_options['yzad3set'] == 'ADFOX') echo "selected='selected'" ?>><?php _e("ADFOX", "rss-for-yandex-zen"); ?></option>
                    </select>
                    <br /><small><?php _e("Рекламная сеть блока рекламы #3.", "rss-for-yandex-zen"); ?> </small>
                </td>
            </tr>
            <tr class="myturbo trrsa3 block3" style="display:none;">
                <th><?php _e("РСЯ идентификатор:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad3rsa" size="22" value="<?php echo stripslashes($yzen_options['yzad3rsa']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор блока РСЯ (например, <strong>RA-123456-7</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
                    <div style="margin-top:30px;"></div>
               </td>
            </tr>
            <tr class="myturbo trfox13 block3" style="display:none;">
                <th><?php _e("ADFOX OwnerId:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad3fox1" size="22" value="<?php echo stripslashes($yzen_options['yzad3fox1']); ?>" />
                    <br /><small><?php _e("Укажите ownerId блока ADFOX (например, <strong>123456</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
               </td>
            </tr>
            <tr class="myturbo trfox23 block3" style="display:none;">
                <th><?php _e("ADFOX идентификатор:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzad3fox2" size="22" value="<?php echo stripslashes($yzen_options['yzad3fox2']); ?>" />
                    <br /><small><?php _e("Укажите идентификатор контейнера ADFOX (например, <strong>adfox</strong>, <a target='new' href='https://yandex.ru/support/webmaster/turbo/find-ad-block.html'>как его узнать</a>)</small>.", "rss-for-yandex-zen"); ?>
                    <div style="margin-top:30px;"></div>
               </td>
            </tr>
            <tr class="myturbo">
                <th><?php _e("Минимальный размер записи:", "rss-for-yandex-zen") ?></th>
                <td>
                    <input type="text" name="yzrazmer" size="22" value="<?php echo stripslashes($yzen_options['yzrazmer']); ?>" />
                    <br /><small><?php _e("Укажите минимальное количество символов записи для добавления рекламы.", "rss-for-yandex-zen"); ?> <br/>
                    <?php _e("Данная опция используется только при вставке рекламы в <strong>середину</strong> записи.", "rss-for-yandex-zen"); ?><br/>
                    <?php _e("Учитывается только текст контента записи (html-разметка не считается).", "rss-for-yandex-zen"); ?>
                    </small>
               </td>
            </tr>
            <tr>
                <th><?php _e("Отключение Турбо:", 'rss-for-yandex-zen') ?></th>
                <td>
                    <label for="yzremoveturbo"><input type="checkbox" value="enabled" name="yzremoveturbo" id="yzremoveturbo" <?php if ($yzen_options['yzremoveturbo'] == 'enabled') echo "checked='checked'"; ?> /><?php _e("Отключить turbo-страницы", "rss-for-yandex-zen"); ?></label>
                    <br /><small><?php _e("Эта опция добавит в RSS-ленту атрибут <tt>turbo=\"false\"</tt> к тегу <tt>&lt;item></tt>.", "rss-for-yandex-zen"); ?> <br />
                    <?php _e("Это единственный способ заставить Яндекс отключить turbo-страницы для вашего сайта.", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Простое отключение опции не поможет - необходимо, чтобы бот Яндекса \"съел\" ленту с <tt>turbo=\"false\"</tt>.", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Функция будет работать только с одновременным включением turbo-страниц (криво, знаю).", "rss-for-yandex-zen"); ?><br />
                    <?php _e("Не используйте эту опцию, если вы вообще не включали поддержку turbo-страниц в плагине.", "rss-for-yandex-zen"); ?>
                    </small>
                </td>
            </tr>

            <tr>
                <th></th>
                <td>
                    <input type="submit" name="submit" class="button button-primary" value="<?php _e('Сохранить настройки &raquo;', 'rss-for-yandex-zen'); ?>" />
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="postbox">
    <h3 style="border-bottom: 1px solid #EEE;background: #f7f7f7;"><span class="tcode"><?php _e('О плагине', 'rss-for-yandex-zen'); ?></span></h3>
	  <div class="inside" style="padding-bottom:15px;display: block;">
     
      <p><?php _e('Если вам нравится мой плагин, то, пожалуйста, поставьте ему <a target="new" href="https://wordpress.org/plugins/rss-for-yandex-zen/"><strong>5 звезд</strong></a> в репозитории.', 'rss-for-yandex-zen'); ?></p>
      <p style="margin-top:20px;margin-bottom:10px;"><?php _e('Возможно, что вам также будут интересны другие мои плагины:', 'rss-for-yandex-zen'); ?></p>
      
      <div class="about">
        <ul>
            <li style="list-style-type: square;margin: 0px 0px 3px 35px;"><a class="yz" target="new" href="https://ru.wordpress.org/plugins/rss-for-yandex-turbo/">RSS for Yandex Turbo</a> - <?php _e('cоздание RSS-ленты для сервиса Яндекс.Турбо.', 'rss-for-yandex-zen'); ?></li>
            <li style="list-style-type: square;margin: 0px 0px 3px 35px;"><a class="yz" target="new" href="https://ru.wordpress.org/plugins/cool-tag-cloud/">Cool Tag Cloud</a> - <?php _e('простое, но очень красивое облако меток.', 'rss-for-yandex-zen'); ?> </li>
            <li style="list-style-type: square;margin: 0px 0px 3px 35px;"><a class="yz" target="new" href="https://ru.wordpress.org/plugins/bbspoiler/">BBSpoiler</a> - <?php _e('плагин позволит вам спрятать текст под тегами [spoiler]текст[/spoiler].', 'rss-for-yandex-zen'); ?></li>
            <li style="list-style-type: square;margin: 0px 0px 3px 35px;"><a class="yz" target="new" href="https://ru.wordpress.org/plugins/easy-textillate/">Easy Textillate</a> - <?php _e('плагин очень красиво анимирует текст (шорткодами в записях и виджетах или PHP-кодом в файлах темы).', 'rss-for-yandex-zen'); ?> </li>
            <li style="list-style-type: square;margin: 0px 0px 3px 35px;"><a class="yz" target="new" href="https://ru.wordpress.org/plugins/cool-image-share/">Cool Image Share</a> - <?php _e('плагин добавляет иконки социальных сетей на каждое изображение в ваших записях.', 'rss-for-yandex-zen'); ?> </li>
            <li style="list-style-type: square;margin: 0px 0px 3px 35px;"><a class="yz" target="new" href="https://ru.wordpress.org/plugins/today-yesterday-dates/">Today-Yesterday Dates</a> - <?php _e('относительные даты для записей за сегодня и вчера.', 'rss-for-yandex-zen'); ?> </li>
            </ul>
      </div>     
    </div>
</div>
<?php wp_nonce_field( plugin_basename(__FILE__), 'yzen_nonce'); ?>
</form>
</div>
</div>
<?php 
}
//функция вывода страницы настроек плагина end

//функция добавления ссылки на страницу настроек плагина в раздел "Настройки" begin
function yzen_menu() {
	add_options_page('Яндекс.Дзен', 'Яндекс.Дзен', 'manage_options', 'rss-for-yandex-zen.php', 'yzen_options_page');
}
add_action('admin_menu', 'yzen_menu');
//функция добавления ссылки на страницу настроек плагина в раздел "Настройки" end

//подключение стилей на странице настроек плагина begin
function yzen_admin_print_scripts() {
    $post_permalink = $_SERVER["REQUEST_URI"];
    if(strpos($post_permalink, 'rss-for-yandex-zen.php') == true) : ?>
        <style>
        tt {padding: 1px 5px 1px;margin: 0 1px;background: #eaeaea;background: rgba(0,0,0,.07);font-size: 13px;font-family: Consolas,Monaco,monospace;unicode-bidi: embed;}
        </style>
    <?php endif; ?>
<?php }    
add_action('admin_head', 'yzen_admin_print_scripts');
//подключение стилей на странице настроек плагина end

//создаем метабокс begin
function yzen_meta_box(){
    $yzen_options = get_option('yzen_options');  
    $yztype = $yzen_options['yztype']; 
    $yztype = explode(",", $yztype);
    add_meta_box('yzen_meta_box', 'Яндекс.Дзен', 'yzen_callback', $yztype, 'normal' , 'high');
}
add_action( 'add_meta_boxes', 'yzen_meta_box' );
//создаем метабокс end

//сохраняем метабокс begin
function yzen_save_metabox($post_id){ 
    global $post;
    
    if ( ! isset( $_POST['yzen_meta_nonce'] ) ) 
        return $post_id;
 
    if ( ! wp_verify_nonce($_POST['yzen_meta_nonce'], plugin_basename(__FILE__) ) )
		return $post_id;
    
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return $post_id;
    
    if(isset($_POST["yzcategory"])){
        $yzcategory = sanitize_text_field($_POST['yzcategory']);
        update_post_meta($post->ID, 'yzcategory_meta_value', $yzcategory);
    }
    if(isset($_POST["yzrating"])){
        $yzrating = 'Да (для взрослых)';
        update_post_meta($post->ID, 'yzrating_meta_value', $yzrating);
    } else {
        $yzrating = 'Нет (не для взрослых)';
        update_post_meta($post->ID, 'yzrating_meta_value', $yzrating);
    }    
    if(isset($_POST["yzrssenabled"])){
        $yzrssenabled = 'yes';
        update_post_meta($post->ID, 'yzrssenabled_meta_value', $yzrssenabled);
    } else {
        $yzrssenabled = 'no';
        update_post_meta($post->ID, 'yzrssenabled_meta_value', $yzrssenabled);
    }     
    
    if(isset($_POST["yzad1meta"])){
        $yzad1meta = 'disabled';
        update_post_meta($post->ID, 'yzad1meta', $yzad1meta);
    } else {
        $yzad1meta = 'enabled';
        update_post_meta($post->ID, 'yzad1meta', $yzad1meta);
    }    
    if(isset($_POST["yzad2meta"])){
        $yzad2meta = 'disabled';
        update_post_meta($post->ID, 'yzad2meta', $yzad2meta);
    } else {
        $yzad2meta = 'enabled';
        update_post_meta($post->ID, 'yzad2meta', $yzad2meta);
    } 
    if(isset($_POST["yzad3meta"])){
        $yzad3meta = 'disabled';
        update_post_meta($post->ID, 'yzad3meta', $yzad3meta);
    } else {
        $yzad3meta = 'enabled';
        update_post_meta($post->ID, 'yzad3meta', $yzad3meta);
    }
}
add_action('save_post', 'yzen_save_metabox');
//сохраняем метабокс end

//выводим метабокс begin
function yzen_callback(){
    global $post;
	wp_nonce_field( plugin_basename(__FILE__), 'yzen_meta_nonce' );
    
    $yzen_options = get_option('yzen_options');
    
    $yzad1meta = get_post_meta($post->ID, 'yzad1meta', true); 
    if (!$yzad1meta) {$yzad1meta = $yzen_options['yzad1'];} 
    
    $yzad2meta = get_post_meta($post->ID, 'yzad2meta', true); 
    if (!$yzad2meta) {$yzad2meta = $yzen_options['yzad2'];} 
    
    $yzad3meta = get_post_meta($post->ID, 'yzad3meta', true); 
    if (!$yzad3meta) {$yzad3meta = $yzen_options['yzad3'];} 
    
    $yzcategory = get_post_meta($post->ID, 'yzcategory_meta_value', true); 
    if (!$yzcategory) {$yzcategory = $yzen_options['yzcategory'];}

    $yzrating = get_post_meta($post->ID, 'yzrating_meta_value', true); 
    if (!$yzrating) {$yzrating = $yzen_options['yzrating'];}   

    $yzrssenabled = get_post_meta($post->ID, 'yzrssenabled_meta_value', true); 
    if (!$yzrssenabled) {$yzrssenabled = "no";}   
    ?>   
    
     <p><strong><?php _e("Тематика:", "rss-for-yandex-zen"); ?></strong>
     <select name="yzcategory">
        <option value="Происшествия" <?php if ($yzcategory == 'Происшествия') echo "selected='selected'" ?>><?php _e("Происшествия", "rss-for-yandex-zen"); ?></option>
        <option value="Политика" <?php if ($yzcategory == 'Политика') echo "selected='selected'" ?>><?php _e("Политика", "rss-for-yandex-zen"); ?></option>
        <option value="Война" <?php if ($yzcategory == 'Война') echo "selected='selected'" ?>><?php _e("Война", "rss-for-yandex-zen"); ?></option>
        <option value="Общество" <?php if ($yzcategory == 'Общество') echo "selected='selected'" ?>><?php _e("Общество", "rss-for-yandex-zen"); ?></option>
        <option value="Экономика" <?php if ($yzcategory == 'Экономика') echo "selected='selected'" ?>><?php _e("Экономика", "rss-for-yandex-zen"); ?></option>
        <option value="Спорт" <?php if ($yzcategory == 'Спорт') echo "selected='selected'" ?>><?php _e("Спорт", "rss-for-yandex-zen"); ?></option>
        <option value="Технологии" <?php if ($yzcategory == 'Технологии') echo "selected='selected'" ?>><?php _e("Технологии", "rss-for-yandex-zen"); ?></option>
        <option value="Наука" <?php if ($yzcategory == 'Наука') echo "selected='selected'" ?>><?php _e("Наука", "rss-for-yandex-zen"); ?></option>
        <option value="Игры" <?php if ($yzcategory == 'Игры') echo "selected='selected'" ?>><?php _e("Игры", "rss-for-yandex-zen"); ?></option>
        <option value="Музыка" <?php if ($yzcategory == 'Музыка') echo "selected='selected'" ?>><?php _e("Музыка", "rss-for-yandex-zen"); ?></option>
        <option value="Литература" <?php if ($yzcategory == 'Литература') echo "selected='selected'" ?>><?php _e("Литература", "rss-for-yandex-zen"); ?></option>
        <option value="Кино" <?php if ($yzcategory == 'Кино') echo "selected='selected'" ?>><?php _e("Кино", "rss-for-yandex-zen"); ?></option>
        <option value="Культура" <?php if ($yzcategory == 'Культура') echo "selected='selected'" ?>><?php _e("Культура", "rss-for-yandex-zen"); ?></option>
        <option value="Мода" <?php if ($yzcategory == 'Мода') echo "selected='selected'" ?>><?php _e("Мода", "rss-for-yandex-zen"); ?></option>
        <option value="Знаменитости" <?php if ($yzcategory == 'Знаменитости') echo "selected='selected'" ?>><?php _e("Знаменитости", "rss-for-yandex-zen"); ?></option>
        <option value="Психология" <?php if ($yzcategory == 'Психология') echo "selected='selected'" ?>><?php _e("Психология", "rss-for-yandex-zen"); ?></option>
        <option value="Здоровье" <?php if ($yzcategory == 'Здоровье') echo "selected='selected'" ?>><?php _e("Здоровье", "rss-for-yandex-zen"); ?></option>
        <option value="Авто" <?php if ($yzcategory == 'Авто') echo "selected='selected'" ?>><?php _e("Авто", "rss-for-yandex-zen"); ?></option>
        <option value="Дом" <?php if ($yzcategory == 'Дом') echo "selected='selected'" ?>><?php _e("Дом", "rss-for-yandex-zen"); ?></option>
        <option value="Хобби" <?php if ($yzcategory == 'Хобби') echo "selected='selected'" ?>><?php _e("Хобби", "rss-for-yandex-zen"); ?></option>
        <option value="Еда" <?php if ($yzcategory == 'Еда') echo "selected='selected'" ?>><?php _e("Еда", "rss-for-yandex-zen"); ?></option>
        <option value="Дизайн" <?php if ($yzcategory == 'Дизайн') echo "selected='selected'" ?>><?php _e("Дизайн", "rss-for-yandex-zen"); ?></option>
        <option value="Фотографии" <?php if ($yzcategory == 'Фотографии') echo "selected='selected'" ?>><?php _e("Фотографии", "rss-for-yandex-zen"); ?></option>
        <option value="Юмор" <?php if ($yzcategory == 'Юмор') echo "selected='selected'" ?>><?php _e("Юмор", "rss-for-yandex-zen"); ?></option>
        <option value="Природа" <?php if ($yzcategory == 'Природа') echo "selected='selected'" ?>><?php _e("Природа", "rss-for-yandex-zen"); ?></option>
        <option value="Путешествия" <?php if ($yzcategory == 'Путешествия') echo "selected='selected'" ?>><?php _e("Путешествия", "rss-for-yandex-zen"); ?></option>
    </select>
    </p>
    
    <p style="margin:5px!important;">
    <label for="yzrating"><input type="checkbox" value="enabled" name="yzrating" id="yzrating" <?php if ($yzrating == 'Да (для взрослых)') echo "checked='checked'"; ?> /><?php _e("Запись с контентом для взрослых", "rss-for-yandex-zen"); ?></label>
<br />
    <label for="yzrssenabled"><input type="checkbox" value="enabled" name="yzrssenabled" id="yzrssenabled" <?php if ($yzrssenabled == 'yes') echo "checked='checked'"; ?> /><?php _e("Исключить эту запись из RSS", "rss-for-yandex-zen"); ?></label>
    </p>
    
    <p style="margin:5px!important;margin-top:10px!important;">
    
    <?php if ($yzen_options['yzad1'] == 'enabled' && $yzen_options['yzturbo'] == 'enabled') { ?>
        <label for="yzad1meta"><input type="checkbox" value="disabled" name="yzad1meta" id="yzad1meta" <?php if ($yzad1meta == 'disabled') echo "checked='checked'"; ?> /><?php _e("Отключить блок рекламы #1 для этой записи (на turbo-страницах)", "rss-for-yandex-zen"); ?></label><br />
    <?php } ?>    
    <?php if ($yzen_options['yzad2'] == 'enabled' && $yzen_options['yzturbo'] == 'enabled') { ?>
        <label for="yzad2meta"><input type="checkbox" value="disabled" name="yzad2meta" id="yzad2meta" <?php if ($yzad2meta == 'disabled') echo "checked='checked'"; ?> /><?php _e("Отключить блок рекламы #2 для этой записи (на turbo-страницах)", "rss-for-yandex-zen"); ?></label><br />
    <?php } ?> 
    <?php if ($yzen_options['yzad3'] == 'enabled' && $yzen_options['yzturbo'] == 'enabled') { ?>
        <label for="yzad3meta"><input type="checkbox" value="disabled" name="yzad3meta" id="yzad3meta" <?php if ($yzad3meta == 'disabled') echo "checked='checked'"; ?> /><?php _e("Отключить блок рекламы #3 для этой записи (на turbo-страницах)", "rss-for-yandex-zen"); ?></label><br />
    <?php } ?> 
    </p>
<?php }
//выводим метабокс end

//добавляем новую rss-ленту begin
function yzen_add_feed(){
    $yzen_options = get_option('yzen_options'); 
    if (!isset($yzen_options['yzrssname'])) {$yzen_options['yzrssname']="zen";update_option('yzen_options', $yzen_options);}
    add_feed($yzen_options['yzrssname'], 'yzen_feed_template');
}
add_action('init', 'yzen_add_feed');
//добавляем новую rss-ленту end

//шаблон для RSS-ленты Яндекс.Дзен begin
function yzen_feed_template(){
$yzen_options = get_option('yzen_options');  

$yztitle = $yzen_options['yztitle'];
$yzlink = $yzen_options['yzlink'];
$yzdescription = $yzen_options['yzdescription'];
$yzlanguage = $yzen_options['yzlanguage']; 
$yznumber = $yzen_options['yznumber']; 
$yztype = $yzen_options['yztype']; 
$yztype = explode(",", $yztype);
$yzfigcaption = $yzen_options['yzfigcaption']; 
$yzimgauthorselect = $yzen_options['yzimgauthorselect']; 
$yzimgauthor = $yzen_options['yzimgauthor']; 
$yzauthor = $yzen_options['yzauthor'];
$yzthumbnail = $yzen_options['yzthumbnail']; 
$yzselectthumb = $yzen_options['yzselectthumb'];  
$yzseodesc = $yzen_options['yzseodesc']; 
$yzseoplugin = $yzen_options['yzseoplugin'];
$yzexcludetags = $yzen_options['yzexcludetags']; 
$yzexcludetagslist = html_entity_decode($yzen_options['yzexcludetagslist']); 
$yzexcludetags2 = $yzen_options['yzexcludetags2']; 
$yzexcludetagslist2 = html_entity_decode($yzen_options['yzexcludetagslist2']); 
$yzexcludecontent = $yzen_options['yzexcludecontent']; 
$yzexcludecontentlist = html_entity_decode($yzen_options['yzexcludecontentlist']);
$tax_query = array();
$yzturbo = $yzen_options['yzturbo']; 
$yzthumbnailturbo = $yzen_options['yzthumbnailturbo']; 
$yzselectthumbturbo = $yzen_options['yzselectthumbturbo']; 
$yzad1 = $yzen_options['yzad1'];
$yzad1mesto = $yzen_options['yzad1mesto'];
$yzad1set = $yzen_options['yzad1set'];
$yzad1rsa = $yzen_options['yzad1rsa'];
$yzad1fox1 = $yzen_options['yzad1fox1'];
$yzad1fox2 = $yzen_options['yzad1fox2'];
$yzad2 = $yzen_options['yzad2'];
$yzad2mesto = $yzen_options['yzad2mesto'];
$yzad2set = $yzen_options['yzad2set'];
$yzad2rsa = $yzen_options['yzad2rsa'];
$yzad2fox1 = $yzen_options['yzad2fox1'];
$yzad2fox2 = $yzen_options['yzad2fox2'];
$yzad3 = $yzen_options['yzad3'];
$yzad3mesto = $yzen_options['yzad3mesto'];
$yzad3set = $yzen_options['yzad3set'];
$yzad3rsa = $yzen_options['yzad3rsa'];
$yzad3fox1 = $yzen_options['yzad3fox1'];
$yzad3fox2 = $yzen_options['yzad3fox2'];
$yzremoveturbo = $yzen_options['yzremoveturbo'];  
$yzrelated = $yzen_options['yzrelated'];
$yzrelatednumber = $yzen_options['yzrelatednumber'];
$yzrelatedselectthumb = $yzen_options['yzrelatedselectthumb']; 

$yzmetrika = $yzen_options['yzmetrika'];
$yzliveinternet = $yzen_options['yzliveinternet'];
$yzgoogle = $yzen_options['yzgoogle'];
$yzmailru = $yzen_options['yzmailru'];
$yzrambler = $yzen_options['yzrambler'];
$yzmediascope = $yzen_options['yzmediascope'];

$yzqueryselect = $yzen_options['yzqueryselect'];
$yztaxlist = $yzen_options['yztaxlist']; 
$yzaddtaxlist = $yzen_options['yzaddtaxlist']; 

if ($yzqueryselect=='Все таксономии, кроме исключенных' && $yztaxlist) {
    $textAr = explode("\n", trim($yztaxlist));
    $textAr = array_filter($textAr, 'trim');
    $tax_query = array( 'relation' => 'AND' );
    foreach ($textAr as $line) {
        $tax = explode(":", $line);
        $taxterm = explode(",", $tax[1]);
        $tax_query[] = array(
            'taxonomy' => $tax[0],
            'field'    => 'id',
            'terms'    => $taxterm,
            'operator' => 'NOT IN',
        );
    } 
}    
if (!$yzaddtaxlist) {$yzaddtaxlist = 'category:10000000';}
if ($yzqueryselect=='Только указанные таксономии' && $yzaddtaxlist) {
    $textAr = explode("\n", trim($yzaddtaxlist));
    $textAr = array_filter($textAr, 'trim');
    $tax_query = array( 'relation' => 'OR' );
    foreach ($textAr as $line) {
        $tax = explode(":", $line);
        $taxterm = explode(",", $tax[1]);
        $tax_query[] = array(
            'taxonomy' => $tax[0],
            'field'    => 'id',
            'terms'    => $taxterm,
            'operator' => 'IN',
        );
    } 
} 

$args = array('ignore_sticky_posts' => 1, 'post_type' => $yztype, 'post_status' => 'publish', 'posts_per_page' => $yznumber,'tax_query' => $tax_query,
'meta_query' => array('relation' => 'OR', array('key' => 'yzrssenabled_meta_value', 'compare' => 'NOT EXISTS',),
array('key' => 'yzrssenabled_meta_value', 'value' => 'yes', 'compare' => '!=',),));
$query = new WP_Query( $args );

header('Content-Type: ' . feed_content_type('rss2') . '; charset=' . get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
do_action( 'rss_tag_pre', 'rss2' );
?>
<rss version="2.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:media="http://search.yahoo.com/mrss/"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:georss="http://www.georss.org/georss"<?php do_action('rss2_ns'); ?>>
<channel>
    <title><?php echo $yztitle; ?></title>
    <link><?php echo $yzlink; ?></link>
    <description><?php echo $yzdescription; ?></description>
    <?php if ($yzturbo == 'enabled' && $yzmetrika) { ?><yandex:analytics id="<?php echo $yzmetrika; ?>" type="Yandex"></yandex:analytics><?php echo PHP_EOL; ?><?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzliveinternet) { ?><yandex:analytics type="LiveInternet"></yandex:analytics><?php echo PHP_EOL; ?><?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzgoogle) { ?><yandex:analytics id="<?php echo $yzgoogle; ?>" type="Google"></yandex:analytics><?php echo PHP_EOL; ?><?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzmailru) { ?><yandex:analytics id="<?php echo $yzmailru; ?>" type="MailRu"></yandex:analytics><?php echo PHP_EOL; ?><?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzrambler) { ?><yandex:analytics id="<?php echo $yzrambler; ?>" type="Rambler"></yandex:analytics><?php echo PHP_EOL; ?><?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzmediascope) { ?><yandex:analytics id="<?php echo $yzmediascope; ?>" type="Mediascope"></yandex:analytics><?php echo PHP_EOL; ?><?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzad1 == 'enabled') { ?>
        <?php if ($yzad1set == 'РСЯ') { ?>
            <yandex:adNetwork type="Yandex" id="<?php echo $yzad1rsa; ?>" turbo-ad-id="first_ad_place"></yandex:adNetwork>
        <?php } ?>
        <?php if ($yzad1set == 'ADFOX') { ?>
            <yandex:adNetwork type="AdFox" turbo-ad-id="first_ad_place">
                <![CDATA[
                    <div id="<?php echo $yzad1fox2; ?>"></div>
                    <script>
                    window.Ya.adfoxCode.create({
                        ownerId: <?php echo $yzad1fox1; ?>,
                        containerId: '<?php echo $yzad1fox2; ?>',
                        params: {
                            pp: 'g',
                            ps: 'cmic',
                            p2: 'fqem'
                        }
                    });
                    </script>
               ]]>
            </yandex:adNetwork>
        <?php } ?>
    <?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzad2 == 'enabled') { ?>
        <?php if ($yzad2set == 'РСЯ') { ?>
            <yandex:adNetwork type="Yandex" id="<?php echo $yzad2rsa; ?>" turbo-ad-id="second_ad_place"></yandex:adNetwork>
        <?php } ?>
        <?php if ($yzad2set == 'ADFOX') { ?>
            <yandex:adNetwork type="AdFox" turbo-ad-id="second_ad_place">
                <![CDATA[
                    <div id="<?php echo $yzad2fox2; ?>"></div>
                    <script>
                    window.Ya.adfoxCode.create({
                        ownerId: <?php echo $yzad2fox1; ?>,
                        containerId: '<?php echo $yzad2fox2; ?>',
                        params: {
                            pp: 'g',
                            ps: 'cmic',
                            p2: 'fqem'
                        }
                    });
                    </script>
               ]]>
            </yandex:adNetwork>
        <?php } ?>
    <?php } ?>
    <?php if ($yzturbo == 'enabled' && $yzad3 == 'enabled') { ?>
        <?php if ($yzad3set == 'РСЯ') { ?>
            <yandex:adNetwork type="Yandex" id="<?php echo $yzad3rsa; ?>" turbo-ad-id="third_ad_place"></yandex:adNetwork>
        <?php } ?>
        <?php if ($yzad3set == 'ADFOX') { ?>
            <yandex:adNetwork type="AdFox" turbo-ad-id="third_ad_place">
                <![CDATA[
                    <div id="<?php echo $yzad3fox2; ?>"></div>
                    <script>
                    window.Ya.adfoxCode.create({
                        ownerId: <?php echo $yzad3fox1; ?>,
                        containerId: '<?php echo $yzad3fox2; ?>',
                        params: {
                            pp: 'g',
                            ps: 'cmic',
                            p2: 'fqem'
                        }
                    });
                    </script>
               ]]>
            </yandex:adNetwork>
        <?php } ?>
    <?php } ?>
    <language><?php echo $yzlanguage; ?></language>
    <generator>RSS for Yandex Zen v1.23 (https://wordpress.org/plugins/rss-for-yandex-zen/)</generator>
    <?php while($query->have_posts()) : $query->the_post(); ?>
    <?php if ($yzturbo == 'enabled') { ?>
        <?php if ($yzremoveturbo != 'enabled') { ?>
            <item turbo="true">
        <?php } else { ?>
            <item turbo="false">
        <?php } ?>
    <?php } else { ?>
    <item>
    <?php } ?>
        <title><?php the_title_rss(); ?></title>
        <link><?php the_permalink_rss(); ?></link>
        <guid><?php the_guid(); ?></guid>
        <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
        <?php $yzrating = get_post_meta(get_the_ID(), 'yzrating_meta_value', true); ?>
        <?php if ($yzrating == 'Да (для взрослых)') { 
            echo '<media:rating scheme="urn:simple">adult</media:rating>'.PHP_EOL;
        } else {
            echo '<media:rating scheme="urn:simple">nonadult</media:rating>'.PHP_EOL;
        } ?>
        <?php if ($yzauthor) { 
            echo '<author>'.$yzauthor.'</author>'.PHP_EOL;
        } else {
            echo '<author>'.get_the_author().'</author>'.PHP_EOL;
        } ?>
        <?php if($yzimgauthorselect == 'Указать автора' && !$yzimgauthor){$yzimgauthor = get_the_author();} ?>
        <?php if($yzimgauthorselect == 'Автор записи'){$yzimgauthor = get_the_author();} ?>
        <?php $yzcategory = get_post_meta(get_the_ID(), 'yzcategory_meta_value', true); ?>
        <?php if ($yzcategory) { echo '<category>'.$yzcategory.'</category>'.PHP_EOL; }
        else {echo '<category>'.$yzen_options['yzcategory'].'</category>'.PHP_EOL;} ?>
        <?php
        if ($yzthumbnail=="enabled" && has_post_thumbnail( get_the_ID() )) {
            echo '<enclosure url="' . strtok(get_the_post_thumbnail_url(get_the_ID(),$yzselectthumb), '?') . '" type="'.yzen_mime_type(strtok(get_the_post_thumbnail_url(get_the_ID(),$yzselectthumb), '?')).'"/>'.PHP_EOL; 
        }    
        $html = yzen_the_content_feed();
        
        if ($yzexcludetags != 'disabled' && $yzexcludetagslist) {
            $html = yzen_strip_tags_without_content($html, $yzexcludetagslist);
        }
        if ($yzexcludetags2 != 'disabled' && $yzexcludetagslist2) {
            $html = yzen_strip_tags_with_content($html, $yzexcludetagslist2, true);
        }
        $html = wpautop($html);

        $dom = new domDocument ('1.0','UTF-8'); 
        @$dom->loadHTML('<?xml version="1.0" encoding="UTF-8"?>' . "\n" . $html);
        $dom->preserveWhiteSpace = false;
        $urltoimages = $dom->getElementsByTagName('a');
        $final  = array();
        foreach ($urltoimages as $urltoimage) {    
            if (yzen_mime_type(strtok($urltoimage->getAttribute('href'), '?'))!="Unknown file type" && ! in_array($urltoimage->getAttribute('href'), $final)) {
                echo '<enclosure url="' . strtok($urltoimage->getAttribute('href'), '?') . '" type="'.yzen_mime_type(strtok($urltoimage->getAttribute('href'), '?')).'"/>'.PHP_EOL; 
                $final[] = $urltoimage->getAttribute('href');
            }    
        }
        $images = $dom->getElementsByTagName('img');    
        $final = array();
        foreach ($images as $image) {         
            if (! in_array($image->getAttribute('src'), $final)) {
                echo '<enclosure url="' . strtok($image->getAttribute('src'), '?') . '" type="'.yzen_mime_type(strtok($image->getAttribute('src'), '?')).'"/>'.PHP_EOL; 
                $final[] = $image->getAttribute('src');
            }    
        }
        ?>
        <?php 
        if ($yzseodesc != 'disabled') { 
            if ($yzseoplugin == 'Yoast SEO') {
                $temp = get_post_meta(get_the_ID(), "_yoast_wpseo_metadesc", true);
                $temp = apply_filters( 'yzen_the_excerpt', $temp );
                $temp = apply_filters( 'convert_chars', $temp );
                $temp = apply_filters( 'ent2ncr', $temp, 8 );
                if (!$temp) {$temp = yzen_the_excerpt_rss();}
                echo "<description><![CDATA[{$temp}]]></description>".PHP_EOL;
            }    
            if ($yzseoplugin == 'All in One SEO Pack') {
                $temp = get_post_meta(get_the_ID(), "_aioseop_description", true);
                $temp = apply_filters( 'yzen_the_excerpt', $temp );
                $temp = apply_filters( 'convert_chars', $temp );
                $temp = apply_filters( 'ent2ncr', $temp, 8 );
                if (!$temp) {$temp = yzen_the_excerpt_rss();}
                echo "<description><![CDATA[{$temp}]]></description>".PHP_EOL;
            }  
        } else { ?>
        <description><![CDATA[<?php echo yzen_the_excerpt_rss(); ?>]]></description>
        <?php } ?>
        <content:encoded><![CDATA[
       	<?php 
		$content = yzen_the_content_feed();
        
        if ($yzexcludetags != 'disabled' && $yzexcludetagslist) {
            $content = yzen_strip_tags_without_content($content, $yzexcludetagslist);
        }
        if ($yzexcludetags2 != 'disabled' && $yzexcludetagslist2) {
            $content = yzen_strip_tags_with_content($content, $yzexcludetagslist2, true);
        }
        
        if ($yzthumbnail=="enabled" && has_post_thumbnail( get_the_ID() )) {
            $image_data = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(),$yzselectthumb),$yzselectthumb);
            $content = '<p><img class="thumb" src="'. strtok(get_the_post_thumbnail_url(get_the_ID(),$yzselectthumb), '?') .'" alt="" width="'.$image_data[1].'" height="'.$image_data[2].'" /></p>' . PHP_EOL . $content;
        }    
        
        //удаляем все unicode-символы (как невалидные в rss)
        $content = preg_replace('/[\x00-\x1F\x7F]/u', '', $content); 
        
        //удаляем все атрибуты тега img кроме alt, src, width, height
        $content = yzen_strip_attributes($content,array('alt','src','width','height'));
        
        $content = wpautop($content);
        
        //удаляем разметку движка при использовании шорткода с подписью [caption] (в html4 темах)
        //и заменяем alt у тега img на текст подписи, установленной в редакторе
        $pattern = "/<div id=\"attachment(.*?)>(.*?)<img(.*?)alt=\"(.*?)\"(.*?) \/>(.*?)<\/p>\n<p class=\"wp-caption-text\">(.*?)<\/p>\n<\/div>/i";
        $replacement = '$2<img$3alt="$7"$5 />$6';
        $content = preg_replace($pattern, $replacement, $content);
        //удаляем ошметки шорткода [caption], если тег <div> удаляется в настройках плагина
        $pattern = "/<p class=\"wp-caption-text\">(.*?)<\/p>/i";
        $replacement = '';
        $content = preg_replace($pattern, $replacement, $content);
        
        //удаляем разметку движка при использовании шорткода с подписью [caption] (в html5 темах)
        //и заменяем alt у тега img на текст подписи, установленной в редакторе
        $pattern = "/<figure id=\"attachment(.*?)>(.*?)<img(.*?)alt=\"(.*?)\"(.*?) \/>(.*?)<figcaption class=\"wp-caption-text\">(.*?)<\/figcaption><\/figure>/i";
        $replacement = '$2<img$3alt="$7"$5 />$6';
        $content = preg_replace($pattern, $replacement, $content);
        
        //удаляем <figure>, если они изначально присутствуют в контенте записи
        $pattern = "/<figure(.*?)>(.*?)<img(.*?)>(.*?)<\/figure>/i";
        $replacement = '<img$3>';
        $content = preg_replace($pattern, $replacement, $content);
        
        //удаление тегов <p> у отдельно стоящих изображений
        $pattern = "/<p><img(.*?)\" \/><\/p>/i";
        $replacement = '<img$1" />';
        $content = preg_replace($pattern, $replacement, $content);
        
        //удаление тегов <p> у отдельно стоящих изображений, обернутых ссылкой
        $pattern = "/<p><a(.*?)><img(.*?)\" \/><\/a><\/p>/i";
        $replacement = '<a$1><img$2" /></a>';
        $content = preg_replace($pattern, $replacement, $content);
        
        //добавляем alt если его вообще нет в теге img
        $pattern = "/<img(?!([^>]*\b)alt=)([^>]*?)>/i";
        $replacement = '<img alt="'. get_the_title_rss() .'"$1$2>';
        $content = preg_replace( $pattern, $replacement, $content );
        
        //устанавливаем alt равным названию записи, если он пустой
        $pattern = "/<img alt=\"\" (.*?) \/>/i";
        $replacement = '<img alt="'.get_the_title_rss().'" $1 />';
        $content = preg_replace($pattern, $replacement, $content);
             
        $copyrighttext = '<span class="copyright">'. $yzimgauthor .'</span>';
        if ($yzimgauthorselect == 'Отключить указание автора') {$copyrighttext = '';}
        $figcaptionopen = '<figcaption>'; $figcaptionclose = '</figcaption>'; 
        if ($yzfigcaption == "Отключить описания" && $yzimgauthorselect == 'Отключить указание автора') {
            $figcaptionopen = '';  $figcaptionclose = ''; 
        }    
       
        //обрабатываем img теги и оборачиваем их тегами figure 
       
        if ($yzfigcaption == "Использовать alt по возможности") {
             //оборачиваем img тегом figure и прописываем ему описание и авторство
             $pattern = "/<img alt=\"(.*?)\" src=\"(.*?)\" \/>/i";
             $replacement = '<figure><img src="$2" />'.$figcaptionopen.'$1'.$copyrighttext.$figcaptionclose.'</figure>';
             $content = preg_replace($pattern, $replacement, $content); 
        } 
        if ($yzfigcaption == "Использовать название записи") {
             //оборачиваем img тегом figure и прописываем ему описание и авторство 
             $pattern = "/<img alt=\"(.*?)\" src=\"(.*?)\" \/>/i";
             $replacement = '<figure><img src="$2" />'.$figcaptionopen.get_the_title_rss() .$copyrighttext.$figcaptionclose.'</figure>';
             $content = preg_replace($pattern, $replacement, $content);
        }  
        if ($yzfigcaption == "Отключить описания") {
             //оборачиваем img тегом figure и прописываем ему описание и авторство 
             $pattern = "/<img alt=\"(.*?)\" src=\"(.*?)\" \/>/i";
             $replacement = '<figure><img src="$2" />'.$figcaptionopen.$copyrighttext.$figcaptionclose.'</figure>';
             $content = preg_replace($pattern, $replacement, $content);
        }     
        
        if ($yzexcludecontent!='disabled' && $yzexcludecontentlist) {
            $textAr = explode("\n", trim($yzexcludecontentlist));
            $textAr = array_filter($textAr, 'trim');
            foreach ($textAr as $line) {
                $line = trim($line);
                $content = preg_replace('/'.$line.'/i','', $content);
            }    
        }    
        
        $content = preg_replace('/<p>https:\/\/youtu.*?<\/p>/i','', $content);
        $content = preg_replace('/<p>https:\/\/www.youtu.*?<\/p>/i','', $content);
    
		echo $content;
        $yzad1meta = get_post_meta(get_the_ID(), 'yzad1meta', true); 
        $yzad2meta = get_post_meta(get_the_ID(), 'yzad2meta', true); 
        $yzad3meta = get_post_meta(get_the_ID(), 'yzad3meta', true); 
		?>]]></content:encoded>
    <?php if ($yzturbo == 'enabled') { ?>
        <turbo:content><![CDATA[
        <header>
                <?php if ($yzthumbnailturbo=="enabled" &&  has_post_thumbnail(get_the_ID()) ) {   
                echo '<figure><img src="'. strtok(get_the_post_thumbnail_url(get_the_ID(),$yzselectthumbturbo), '?') .'" /></figure>'.PHP_EOL;} ?>
        <h1><?php the_title_rss(); ?></h1>
        </header>
        <?php if ($yzturbo == 'enabled' && $yzad1 == 'enabled' && $yzad1mesto == 'После заголовка записи' && $yzad1meta != 'disabled') { ?>
            <figure data-turbo-ad-id="first_ad_place"></figure>
        <?php } ?>
        <?php if ($yzturbo == 'enabled' && $yzad2 == 'enabled' && $yzad2mesto == 'После заголовка записи' && $yzad2meta != 'disabled') { ?>
            <figure data-turbo-ad-id="second_ad_place"></figure>
        <?php } ?>
        <?php if ($yzturbo == 'enabled' && $yzad3 == 'enabled' && $yzad3mesto == 'После заголовка записи' && $yzad3meta != 'disabled') { ?>
            <figure data-turbo-ad-id="third_ad_place"></figure>
        <?php } ?>
        <?php 
        if ($yzthumbnail=="enabled" && has_post_thumbnail( get_the_ID() )) {
            $pattern = "/<figure>(.*?)<\/figure>/i";
            $replacement = '';
            $content = preg_replace($pattern, $replacement, $content, 1);
        }
        ?>
        <?php echo yzen_strip_attributes(yzen_add_advert($content),array('alt','src')); ?>
        <?php if ($yzturbo == 'enabled' && $yzad1 == 'enabled' && $yzad1mesto == 'В конце записи' && $yzad1meta != 'disabled') { ?>
            <figure data-turbo-ad-id="first_ad_place"></figure>
        <?php } ?>
        <?php if ($yzturbo == 'enabled' && $yzad2 == 'enabled' && $yzad2mesto == 'В конце записи' && $yzad2meta != 'disabled') { ?>
            <figure data-turbo-ad-id="second_ad_place"></figure>
        <?php } ?>
        <?php if ($yzturbo == 'enabled' && $yzad3 == 'enabled' && $yzad3mesto == 'В конце записи' && $yzad3meta != 'disabled') { ?>
            <figure data-turbo-ad-id="third_ad_place"></figure>
        <?php } ?>
        ]]></turbo:content>
        <?php 
        if ( $yzrelated=="enabled"  ) {   
            
            $cats = array();
            foreach (get_the_category(get_the_ID()) as $c) {
                $cat = get_category($c);
                array_push($cats, $cat->cat_ID);
            }
            $cur_post_id = array();
            array_push($cur_post_id, get_the_ID());

            $args = array('post__not_in' => $cur_post_id, 'cat' => $cats,'orderby' => 'rand','ignore_sticky_posts' => 1, 'post_type' => $yztype, 'post_status' => 'publish', 'posts_per_page' => $yzrelatednumber,'tax_query' => $tax_query,'meta_query' => array('relation' => 'OR', array('key' => 'yzrssenabled_meta_value', 'compare' => 'NOT EXISTS',),array('key' => 'yzrssenabled_meta_value', 'value' => 'yes', 'compare' => '!=',),));
            $related = new WP_Query( $args );
            
            if (!$related->have_posts()) {
                $args = array('post__not_in' => $cur_post_id, 'orderby' => 'rand','ignore_sticky_posts' => 1, 'post_type' => $yztype, 'post_status' => 'publish', 'posts_per_page' => $yzrelatednumber,'tax_query' => $tax_query,'meta_query' => array('relation' => 'OR', array('key' => 'yzrssenabled_meta_value', 'compare' => 'NOT EXISTS',),array('key' => 'yzrssenabled_meta_value', 'value' => 'yes', 'compare' => '!=',),));
                $related = new WP_Query( $args );
            }    

            if ($related->have_posts()) {echo '<yandex:related>'.PHP_EOL;}
            while ($related->have_posts()) : $related->the_post();
                $thumburl = '';
                if ($yzrelatedselectthumb != "Не использовать" && has_post_thumbnail(get_the_ID()) ) {
                    $thumburl = ' img="' . strtok(get_the_post_thumbnail_url(get_the_ID(),$yzrelatedselectthumb), '?') . '"';
                }    
                $rlink = htmlspecialchars(get_the_permalink());
                $rtitle = get_the_title_rss();
                if ($yzrelatedselectthumb != "Не использовать") {
                    echo '<link url="'.$rlink.'"'.$thumburl.'>'.$rtitle.'</link>'.PHP_EOL;
                } else {
                    echo '<link url="'.$rlink.'">'.$rtitle.'</link>'.PHP_EOL;
                }
            
            endwhile;
            if ($related->have_posts()) {echo '</yandex:related>'.PHP_EOL;}
            wp_reset_query($related);
        } ?>
    <?php } ?>
        <?php rss_enclosure(); ?>
    </item>
<?php endwhile; ?>
<?php wp_reset_query(); ?>
</channel>
</rss>
<?php }
//шаблон для RSS-ленты Яндекс.Дзен end

//функция установки корректного mime type для изображений begin
function yzen_mime_type($file) {
	$mime_type = array(
		"bmp"			=>	"image/bmp",
		"gif"			=>	"image/gif",
		"ico"			=>	"image/x-icon",
		"jpeg"			=>	"image/jpeg",
		"jpg"			=>	"image/jpeg",
		"png"			=>	"image/png",
		"psd"			=>	"image/vnd.adobe.photoshop",
		"svg"			=>	"image/svg+xml",
		"tiff"			=>	"image/tiff",
		"webp"			=>	"image/webp",
	);
	$extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
	if (isset($mime_type[$extension])) {
		return $mime_type[$extension];
	} else {
		return "Unknown file type";
	}
}
//функция установки корректного mime type для изображений end

//установка правильного content type для ленты плагина begin
function yzen_feed_content_type( $content_type, $type ) {
    $yzen_options = get_option('yzen_options'); 
    if (!isset($yzen_options['yzrssname'])) {$yzen_options['yzrssname']="zen";update_option('yzen_options', $yzen_options);}
    if( $yzen_options['yzrssname'] == $type ) {
        $content_type = 'application/rss+xml';
    }
    return $content_type;
}
add_filter( 'feed_content_type', 'yzen_feed_content_type', 10, 2 );
//установка правильного content type для ленты плагина end

//функция формирования description в rss begin
function yzen_the_excerpt_rss() {
    $content = get_the_excerpt();
    $content = apply_filters('yzen_the_excerpt', $content);
    $content = apply_filters('convert_chars', $content);
    $content = apply_filters('ent2ncr', $content, 8);
    return $content;
}
//функция формирования description в rss end

//функция формирования content в rss begin
function yzen_the_content_feed() {
    $content = apply_filters('the_content', get_post_field('post_content', get_the_ID()));
    $content = do_shortcode($content);
    $content = apply_filters('yzen_the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
    $content = apply_filters('wp_staticize_emoji', $content);
    $content = apply_filters('_oembed_filter_feed_content', $content);
    return $content;
}
//функция формирования content в rss end

//функция удаления тегов вместе с их контентом begin 
function yzen_strip_tags_with_content($text, $tags = '', $invert = FALSE) {

  preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
  $tags = array_unique($tags[1]);
   
  if(is_array($tags) AND count($tags) > 0) {
    if($invert == FALSE) {
      return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    else {
      return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
    }
  }
  elseif($invert == FALSE) {
    return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
  }
  return $text;
} 
//функция удаления тегов вместе с их контентом end

//функция удаления тегов без их контента begin 
function yzen_strip_tags_without_content($text, $tags = '') {

    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);
   
    if(is_array($tags) AND count($tags) > 0) {
        foreach($tags as $tag)  {
            $text = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/", '', $text);
        }
    }
    return $text;
} 
//функция удаления тегов без их контента end 

//функция принудительной установки header-тега X-Robots-Tag (решение проблемы с SEO-плагинами) begin
function yzen_index_follow_rss() {
    $yzen_options = get_option('yzen_options'); 
    if (!isset($yzen_options['yzrssname'])) {$yzen_options['yzrssname']="zen";update_option('yzen_options', $yzen_options);}
    if ( is_feed( $yzen_options['yzrssname'] ) ) {
        header( 'X-Robots-Tag: index, follow', true );
    }
}
add_action( 'template_redirect', 'yzen_index_follow_rss', 999999 );
//функция принудительной установки header-тега X-Robots-Tag (решение проблемы с SEO-плагинами) end

//функция добавления рекламы в середину записи begin
function yzen_add_advert( $content ){
    
    $yzen_options = get_option('yzen_options');  
    $yzrazmer = $yzen_options['yzrazmer'];
    $yzad1 = $yzen_options['yzad1'];
    $yzad1mesto = $yzen_options['yzad1mesto'];
    $yzad2 = $yzen_options['yzad2'];
    $yzad2mesto = $yzen_options['yzad2mesto'];
    $yzad3 = $yzen_options['yzad3'];
    $yzad3mesto = $yzen_options['yzad3mesto'];
    
    $tempcontent = $content;
    $tempcontent = strip_tags($tempcontent);
    $tempcontent = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $tempcontent);

    $num = ceil(mb_strlen($tempcontent) / 2);
    $temp = ceil(mb_strlen($tempcontent) / 10);
    $num = $num - $temp;
    
    global $post;
    $yzad1meta = get_post_meta($post->ID, 'yzad1meta', true); 
    $yzad2meta = get_post_meta($post->ID, 'yzad2meta', true); 
    $yzad3meta = get_post_meta($post->ID, 'yzad3meta', true);    

    if (mb_strlen($tempcontent) < (int)$yzrazmer) {return $content;}
    if ( $yzad1mesto != 'В середине записи' && $yzad2mesto != 'В середине записи' && $yzad3mesto != 'В середине записи') {return $content;}
    
    if ($yzad1 == 'enabled' && $yzad1mesto == 'В середине записи' && $yzad1meta != 'disabled') {
        $ads = PHP_EOL.'<figure data-turbo-ad-id="first_ad_place"></figure>';
    }
    elseif ($yzad2 == 'enabled' && $yzad2mesto == 'В середине записи' && $yzad2meta != 'disabled') {
        $ads = PHP_EOL.'<figure data-turbo-ad-id="second_ad_place"></figure>';
    }    
	elseif ($yzad3 == 'enabled' && $yzad3mesto == 'В середине записи' && $yzad3meta != 'disabled') {
        $ads = PHP_EOL.'<figure data-turbo-ad-id="third_ad_place"></figure>';
    } 
    else {
        return $content;
    }

	return preg_replace('~[^^]{'. $num .'}.*?(?:\r?\n\r?\n|</p>|</figure>|</ul>|</pre>|</table>)~su', "\${0}$ads", trim( $content ), 1);
}
//функция добавления рекламы в середину записи end

//функция удаления всех атрибутов тега img кроме указанных begin
function yzen_strip_attributes($s, $allowedattr = array()) {
  if (preg_match_all("/<img[^>]*\\s([^>]*)\\/*>/msiU", $s, $res, PREG_SET_ORDER)) {
   foreach ($res as $r) {
     $tag = $r[0];
     $attrs = array();
     preg_match_all("/\\s.*=(['\"]).*\\1/msiU", " " . $r[1], $split, PREG_SET_ORDER);
     foreach ($split as $spl) {
      $attrs[] = $spl[0];
     }
     $newattrs = array();
     foreach ($attrs as $a) {
      $tmp = explode("=", $a);
      if (trim($a) != "" && (!isset($tmp[1]) || (trim($tmp[0]) != "" && !in_array(strtolower(trim($tmp[0])), $allowedattr)))) {

      } else {
          $newattrs[] = $a;
      }
     }
    
     //сортировка чтобы alt был раньше src   
     sort($newattrs);
     reset($newattrs);
     
     $attrs = implode(" ", $newattrs);
     $rpl = str_replace($r[1], $attrs, $tag);
     //заменяем одинарные кавычки на двойные
     $rpl = str_replace("'", "\"", $rpl);   
     
     //добавляем закрывающий символ / если он отсутствует
     $rpl = str_replace("\">", "\" />", $rpl);
     //добавляем пробел перед закрывающим символом /
     $rpl = str_replace("\"/>", "\" />", $rpl);
     
     //удаляем двойные пробелы
     $rpl = str_replace("  ", " ", $rpl);
    
     //выносим атрибут height в конец тега   
     $pattern = '/<img(.*?) height="(.*?)" (.*?) \/>/i';
     $replacement = '<img$1 $3 height="$2" />';
     $rpl = preg_replace($pattern, $replacement, $rpl);
     
     $s = str_replace($tag, $rpl, $s);
   }
  } 

  return $s;
}
//функция удаления всех атрибутов тега img кроме указанных end
